@echo off
rem $Id$
rem *********************************************************************************************
rem ** Shell wrapper for Windows based systems - modified from phing.bat CVS revision 143
rem *********************************************************************************************

echo Drupal Automated Staging Toolkit                 
echo --------------------------------

if "%OS%"=="Windows_NT" @setlocal 
if not "%OS%"=="Windows_NT" goto no_WinNT
if "%1" =="" goto no_buildfile

rem %~dp0 is expanded pathname of the current script under NT
set DAST_HOME=%~dp0..

:init
if "%PHP_COMMAND%" == "" goto no_phpcommand
goto run
goto cleanup

:run
rem echo %PHP_COMMAND% -d html_errors=off -qC %DAST_HOME%\bin\dast.php %1 %2 %3 %4 %5 %6 %7 %8 %9
%PHP_COMMAND% -d html_errors=off -qC "%DAST_HOME%\bin\dast.php" %1 %2 %3 %4 %5 %6 %7 %8 %9
goto cleanup

:no_phpcommand
echo ------------------------------------------------------------------------
echo WARNING: Set environment var PHP_COMMAND to the location of your php.exe
echo          executable (e.g. C:\PHP\php.exe). 
echo Assuming php.exe is on path...
echo ------------------------------------------------------------------------
set PHP_COMMAND=php.exe
goto init

:no_buildfile
echo ERROR: You must specify a build file.

:no_WinNT
This script requires Windows NT.

:cleanup
if "%OS%"=="Windows_NT" @endlocal

REM pause
