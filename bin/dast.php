<?php
 * @file
 * Command line launcher for DAST
 * 
 * This is a modified version of the Phing command line launcher based on phing/bin/phing.php CVS revision 1.7. 
 * It starts up the system evironment, tests for all important paths and properties and kicks of the main command-
 * line entry point of phing located in phing.Phing
 * @author Allister Beharry
 * @package org.drupal.dast
 */

/* Check OS */
if ( substr_count(php_uname(), 'Windows') > 0 ) {
  if ( substr_count(php_uname(), 'Windows NT') <= 0 ) {
    die ("ERROR: This script requires Windows NT. \n" . " File: " . __FILE__ . " on line: " . __LINE__);
  }
} 

/* Check min. required PHP version */
$ver = explode( '.', PHP_VERSION );
$ver_num = $ver[0] . $ver[1] . $ver[2];
if ( $ver_num < 520 ) die ('ERROR: This script needs PHP 5.2.0 or above!' );

/* Set any INI options for PHP */
ini_set('track_errors', 1); 

define('DAST_HOME', realpath(realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR));
define('DAST_TEST', DAST_HOME.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'tests');
define('DAST_TEMP', DAST_HOME.DIRECTORY_SEPARATOR.'tmp');
define('PHING_TEST_BASE', DAST_HOME.DIRECTORY_SEPARATOR.'src');

$args = isset($argv) ? $argv : $_SERVER['argv']; // $_SERVER['argv'] seems not to work when argv is registered (PHP5b4)
/* Shift CLI arguments so script name falls off*/
array_shift($args);

for ($i = 0, $argcount = count($args); $i < $argcount; $i++) { 
  $arg = $args[$i];
  
  /* help parameter */
  if ($arg =='-h' || $arg=='-help' || $arg =='--help') {
$msg="Usage: dast -p [BUILDPROJECT]
       dast -p [BUILDPROJECT] -D[\"PROPERTYASSIGN...\"]
       dast -p [BUILDPROJECT] -D[\"PROPERTYASSIGN...\"] [OPTION...]
       dast -f [BUILDFILE] -D[\"PROPERTYASSIGN...\"] [OPTION...]

DAST is a toolkit based on the Phing build system for automating the
process of bootstrapping a Drupal site. DAST treats the process of creating
an instance of the Drupal CMS - downloading the core and module code, creating
the database and configuring PHP, the web and database server; and running
the Drupal installer with an installation profile, as a software build process
which can be automated using a formal build framework like Phing, and therefore
removes the manual and tedious process of setting up a new instance
of Drupal for developers, testers, and users. \n 
Developer and tesers can use DAST to quickly create a site with a particular 
configuration when developing  a module or testing code. End-users and newbies
can also used DAST as a quick way of setting up Drupal with the modules 
required for a particular installation profile. With a DAST build-project you 
can specify the CVS revision of core you want to use (e.g. DRUPAL-5-2) a list 
of modules you want to use and their CVS revisions, the name and location of 
the database you want to use with the Drupal site, and the installation 
profile you want to run during site installation \n
Examples: \n
 #Run the do_testing-site build project in DAST_HOME/projects/do_testing-site
 dast -p do_testing-site 
  
 #Run the default-site build project using the properties file mysite.properties
 dast -p default-site -D\"propertiesFile=mysite.properties\"
 
 #Run the patchtest-site build project and log all output to ./log/patchtest-site.log
 dast -p patchtest-site -logfile ./log/patchtest-site.log

 #Run the site-patch core build file using base and user properties
 dast -f site-patch.xml -D\"basePropertiesFile=mysite-base.properties\" 
                        -D\"propertiesFile=mysite.properties\"
";


    echo ("$msg \n");
    exit(0);
  }
  
  /* build file parameter */
  if ($arg == '-buildfile' || $arg == '--buildfile' || $arg == '-file' || $arg == '--file' || $arg == '-f') {
    if (!isset($args[$i+1])) 
      die("ERROR: You must specify a buildfile when using the -f or -file -buildfile argument.\n"); //No file arg specified
    
    if (is_file($args[$i+1])) {
      if (!file_exists(realpath($args[$i+1]))) //next argument is build file 
        die("ERROR: Could not find build file ".$args[$i+1].".\n");
      else {
        echo "Using build file: ".$args[$i+1]."\n";
        $args[$i+1] = realpath($args[$i+1]);
      } //else
    }// if is_file
  }// arg = buildfile

  /*project parameter*/
  if ($arg == '-p' || $arg =='-project' || $arg =='--project') {
    if (!isset($args[$i+1])) 
      die("ERROR: You must specify a project name when using the -p or --project argument.\n"); //No project arg specified
    else {
      $projectdir = realpath(DAST_HOME.DIRECTORY_SEPARATOR.'projects'.DIRECTORY_SEPARATOR.$args[$i+1].DIRECTORY_SEPARATOR);
      if (!is_dir($projectdir))
        die ("ERROR: Could not find directory ".DAST_HOME.DIRECTORY_SEPARATOR.'projects'.DIRECTORY_SEPARATOR.$args[$i+1].DIRECTORY_SEPARATOR. "\n");
      $projectfile = $projectdir.DIRECTORY_SEPARATOR.$args[$i+1].'.xml';
      if (!file_exists(realpath($projectfile))) 
        die("ERROR: Could not find build file $projectfile.\n");
      else {
        echo "Using build file: $projectfile \n";
        $args[$i] = '-f';
        $args[$i+1] = "$projectfile";
      } //else  
    } // else
  } //if arg == p


}// for $i

echo PHP_EOL; 
/* Make sure all required libraries are present and accounted for */
echo "Using DAST_HOME: ".DAST_HOME."...\n";
if (file_exists(DAST_HOME.DIRECTORY_SEPARATOR.'phing'))
  echo "Phing found...\n";
else
  die ("ERROR: Phing not found at ".DAST_HOME.DIRECTORY_SEPARATOR.'phing'."- see README.txt on installing Phing for DAST.\n" . " File: " . __FILE__ . " on line: " . __LINE__);
if (file_exists(DAST_HOME.DIRECTORY_SEPARATOR.'creole'))
  echo "Creole found...\n";
else
  die ("ERROR: Creole not found - see README.txt on installing Creole for DAST.\n" . " File: " . __FILE__ . " on line: " . __LINE__); 
if (file_exists(DAST_HOME.DIRECTORY_SEPARATOR.'jargon'))
  echo "Jargon found...\n";
else
  die ("ERROR: Jargon not found - see README.txt on installing Jargon for DAST.\n" . " File: " . __FILE__ . " on line: " . __LINE__);
if (file_exists(DAST_HOME.DIRECTORY_SEPARATOR.'simpletest'))
  echo "Simpletest found...\n";
else
  die ("ERROR: Simpletest not found - see README.txt on installing Simpletest for DAST.\n" . " File: " . __FILE__ . " on line: " . __LINE__);
if (file_exists(DAST_HOME.DIRECTORY_SEPARATOR.'PEAR_Config'))
  echo "PEAR::Config (bundled) found...\n";
else
  die ("ERROR: PEAR::Config not found - see README.txt on installing PEAR::Config for DAST.\n" . " File: " . __FILE__ . " on line: " . __LINE__);
echo PHP_EOL;
//@TODO Detect and allow the use of libraries installed through PEAR or anywhere on the include_path.

/* set classpath - we want to use our own versions of phing and creole and jargon and simpletest so in PHP_CLASSPATH
 * make sure the libraries bundled with dast are included first
*/
define ('DAST_CLASSPATH', DAST_HOME.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'src'.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'phing'.
         DIRECTORY_SEPARATOR.'classes'.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'PHPUnit');

if (getenv('PHP_CLASSPATH')) {
    define('PHP_CLASSPATH',  
     DAST_CLASSPATH.PATH_SEPARATOR.getenv('PHP_CLASSPATH') . PATH_SEPARATOR . get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
} else {
    define('PHP_CLASSPATH',  DAST_CLASSPATH.PATH_SEPARATOR.get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
}

echo "Using PHP_CLASSPATH: ".PHP_CLASSPATH."\n";
echo PHP_EOL;
require_once 'phing/Phing.php';

/* Setup Phing environment */
Phing::startup();

/* Set DAST home directory */
Phing::setProperty('dast.home', DAST_HOME);

/* Set Phing home directory */
Phing::setProperty('phing.home', DAST_HOME.DIRECTORY_SEPARATOR.'phing');

/* Set DAST tmp directory */
Phing::setProperty('dast.tmp', DAST_HOME.DIRECTORY_SEPARATOR.'tmp');

/* Set DAST ext directory */
Phing::setProperty('dast.ext', DAST_HOME.DIRECTORY_SEPARATOR.'ext');

/*Set PHP DIRECTORY_SEPARATOR property - needed for any filesystem ops */
Phing::setProperty('php.directory_separator', DIRECTORY_SEPARATOR);
Phing::setProperty('php.path_separator', PATH_SEPARATOR);



/* fire main application */
Phing::fire($args);

/*
  exit OO system if not already called by Phing
   -- basically we should not need this due to register_shutdown_function in Phing
 */
Phing::halt(0);