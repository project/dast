--This script drops and create a database with a user and privileges for use with a Drupal  site
--The variables ${drupal.database.user}, ${drupal.database.user.pass}, ${drupal.database.user.pass}, ${drupal.database.name}, ${drupal.database.host} 
--represent placeholders that will be replaced by literal values from the properties set in the site-install and site-configure build script.

--Revoke all privileges for the user 
DELETE FROM `tables_priv` where `User` = '${drupal.database.user}' and 'Host' = '${drupal.database.user.host}';
DELETE FROM `columns_priv` where `User` = '${drupal.database.user}' and 'Host' = '${drupal.database.user.host}';
FLUSH PRIVILEGES;
SELECT "Deleted any existing privileges for user ${drupal.database.user}@${drupal.database.user.host}" as MSG;

--Drop the user if it exists
DELETE FROM `user` where `User`= '${drupal.database.user}' and `Host` = '${drupal.database.user.host}';
SELECT "Deleted (if existed) user ${drupal.database.user}@${drupal.database.user.host}..." as MSG;

--Drop database if it exists
DROP DATABASE IF EXISTS ${drupal.database.name};
SELECT "Deleted (if existed) database ${drupal.database.name}..." as MSG;

--Create database
CREATE DATABASE ${drupal.database.name};
SELECT "Created database ${drupal.database.name}..." as MSG;

--Grant privileges
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE
  TEMPORARY TABLES, LOCK TABLES
ON ${drupal.database.name}.*
TO '${drupal.database.user}'@'${drupal.database.user.host}' IDENTIFIED BY '${drupal.database.user.pass}';
FLUSH PRIVILEGES;
SELECT "Granted privileges SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES to user ${drupal.database.user}@${drupal.database.user.host}.." as MSG;
