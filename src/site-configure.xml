<?xml version="1.0" encoding="UTF-8"?>
<!-- <![CDATA[ 
]]> -->

<!-- ==================================================================================================== -->
<!--  @file This is the base site-configure task in DAST. It writes any required directives for hosting   -->                                   
<!--  the Drupal site to web, database and PHP/application server config files and creates the database.  -->
<!--  It will also scan for and report if the required directives are set within the recommended limits   -->                  
<!--  @author Allister Beharry                                                                            -->
<!--  @package org.drupal.dast.tasks                                                                      -->
<!-- ==================================================================================================== -->

<project name="site-configure" default="main">  
  <taskdef name="CreoleSQLExec" classname="phing.tasks.ext.CreoleSQLExecTask"/>
  <taskdef name="CVS" classname="phing.tasks.system.CVSTask"/>
  <taskdef name="warn" classname="phing.tasks.system.WarnTask"/>
  <taskdef name="DumpProperties" classname="DumpPropertiesTask" />
  <taskdef name="ApacheHttpdConf" classname="ApacheHttpdConfTask" />
  <typedef name="ApacheHttpdConfSection" classname="ApacheHttpdConfSection" />
  <typedef name="ApacheHttpdConfDirective" classname="ApacheHttpdConfDirective" />
  

 <!-- ================================= 
          target: create-database              
         ================================= -->
  <target name="create-database" depends="configure-create-database" description="--> Creates the database for the Drupal site">
    <!--Copy script file template to tmp directory replacing tokens with property values -->
    <copy todir="${dast.tmp}" overwrite="true">
      <filterchain>
        <expandproperties />
      </filterchain>
      <fileset dir="${dast.ext}">
        <include name="${drupal.database.CreateScript}" />
      </fileset>
    </copy>
    
    <!--Recreate drupal db and user -->
    <echo>Running ${drupal.database.CreateScript} on ${drupal.database.CreateUrl}...</echo>
    <CreoleSQLExec Url="${drupal.database.CreateUrl}" Print="true" Showheaders="false" Src="${dast.tmp}${php.directory_separator}${drupal.database.CreateScript}"/>

    <!--Check if drupal database url is now valid-->
    <echo>Checking connection to ${drupal.database.Url}...</echo>
    <CreoleSQLExec Url="${drupal.database.Url}" Print="true" Showheaders="false">
     SELECT "${drupal.database.Url} is now available." as MSG;
    </CreoleSQLExec>
    
    <!--Keep db script file if testing -->
    <if><not><equals arg1="${build.testing}" arg2="yes" /></not><then>
      <delete file="${dast.tmp}${php.directory_separator}${drupal.database.CreateScript}" />
    </then></if>  
  </target>
  
  <!--Create the files subdirectory and set permission-->
  <!-- ================================= 
          target: create-files-dir              
         ================================= -->
  <target name="create-files-dir" depends="configure-create-files-dir" description="--> Create the Drupal files directory ">
    <mkdir dir="${drupal.filesDirPath}"/>
    <echo> Created directory ${drupal.filesDirPath}.</echo>
    <!--
    <php function = "chmod" returnProperty="chmodfilesDirResult" >
      <param value = "${drupal.filesDirPath}" /> 
      <param value = "${drupal.filesDirPermision}" />
    </php> 
    <if><equals arg1="${build.testing}" arg2="yes" /><then>
      <echo>PHP chmod returned: ${chmodfilesDirResult}</echo></then>
    </if>
    <if><equals arg1="${chmodfilesDirResult}" arg2="1"/><then>
      <echo>chmoded ${drupal.filesDirPath} to ${drupal.filesDirPermission}.</echo></then>
    <else>
      <warn>Could not chmod ${drupal.filesDirPath} to ${drupal.filesDirPermission}. You will have to change permissions on this directory manually.</warn>
    </else>
    </if>  
    -->
    <!--
    <if><or><equals arg1="${host.fstype}" arg2="UNIX" /><equals arg1="${build.usechmod}" arg2="yes" /></or><then>
      <exec command="chmod ${drupal.filesDirPermission} &quot;${drupal.filesDirPath}&quot;" 
        Output="${build.log.defaultdir}${php.directory_separator}site-configure-create-filesdir-output.log" Error="${build.log.defaultdir}${php.directory_separator}site-configure-create-filesdir.log" 
        dir="${dast.tmp}" escape="false" passthru="false" checkreturn="true"/>
      <echo>chmoded ${drupal.filesDirPath} to ${drupal.filesDirPermission}.</echo>
    </then>
    <else>
      <warn>Changing permissions on the files directory only works under UNIX-type filesystems; you will have to change permissions on ${drupal.filesDirPath} manually</warn>
    </else>
    </if>
    -->
    <!--Make files dir writeable-->
    <Chmod Mode="0777">
      <fileset dir="${drupal.filesDirPath}/">
        <include name="**" />
      </fileset>
    </Chmod>  
  </target>

  <!-- ================================= 
          target: create-apachehttpd-site              
         ================================= -->
  <target name="create-apachehttpd-site" depends="configure-create-apachehttpd-site" 
    description="--> This task will create a new Apache httpd site; modify the files and directives written here to suit your httpd setup ">
     
  <!--Add the port for the our site vhost to ports.conf -->
  <ApacheHttpdConf File="${apachehttpd.ports.conf}">
    <ApacheHttpdConfDirective DirectiveName="Listen" DirectiveContent="${apachehttpd.site.port}" DirectiveOverwrite="no"/>
  </ApacheHttpdConf>
    
  <!--Make sure the vhost configs in sites-enabled dirs and ports.config is being pulled in in apache2.conf -->
  <ApacheHttpdConf File="${apachehttpd.apache2.conf}">
    <ApacheHttpdConfDirective DirectiveName="Include" DirectiveContent="${apachehttpd.ports.conf}" DirectiveOverwrite="yes" />
    <ApacheHttpdConfDirective DirectiveName="Include" DirectiveContent="${apachehttpd.sites-enabled.dir}" DirectiveOverwrite="yes" />   
      
  </ApacheHttpdConf>   
      
  <!--Create the vhost in a conf file at "${apachehttpd.site.vhost.conf} --> 
  <ApacheHttpdConf File="${apachehttpd.site.vhost.conf}">
    <ApacheHttpdConfSection SectionType="VirtualHost" SectionName="*:${apachehttpd.site.port}">
      <ApacheHttpdConfDirective DirectiveName="ServerAdmin" DirectiveContent="${apachehttpd.site.vhost.ServerAdmin}" />
      <ApacheHttpdConfDirective DirectiveName="DocumentRoot" DirectiveContent="${apachehttpd.site.vhost.DocumentRoot}" />
      <ApacheHttpdConfDirective DirectiveName="ServerName" DirectiveContent="${apachehttpd.site.vhost.ServerName}" />
      <ApacheHttpdConfDirective DirectiveName="ErrorLog" DirectiveContent="${apachehttpd.site.vhost.ErrorLog}" />
      <ApacheHttpdConfDirective DirectiveName="CustomLog" DirectiveContent="${apachehttpd.site.vhost.CustomLog}" />
      
      <!--Set our vhost / directory settings -->
      <ApacheHttpdConfSection SectionType="Directory" SectionName = "">
        <ApacheHttpdConfDirective DirectiveName="Options" DirectiveContent="FollowSymLink MultiViews" />
        <ApacheHttpdConfDirective DirectiveName="AllowOverride" DirectiveContent="None" />
      </ApacheHttpdConfSection>
      
      <!--Then our DocumentRoot directory settings -->
      <ApacheHttpdConfSection SectionType="Directory" SectionName="${apachehttpd.site.vhost.DocumentRoot}">
        <ApacheHttpdConfDirective DirectiveName="Options" DirectiveContent="Options Indexes FollowSymLink MultiViews" />
        <ApacheHttpdConfDirective DirectiveName="AllowOverride" DirectiveContent="None" />
        <ApacheHttpdConfDirective DirectiveName="Order" DirectiveContent="allow,deny" />
        <ApacheHttpdConfDirective DirectiveName="Allow" DirectiveContent="from all" />      
      </ApacheHttpdConfSection>
      
    </ApacheHttpdConfSection>
  </ApacheHttpdConf>
  
  <!--Set our vdir directory properties -->  
  <ApacheHttpdConf File="${apachehttpd.site.vhost.conf}">
    
  </ApacheHttpdConf>
       
    </target>

  <!-- ================================= 
          target: main              
         ================================= -->
  <target name="main" depends="configure" description="--> description">
        
  </target>
  
  <!-- ================================= 
          target: configure              
         ================================= -->
  <target name="configure" description="--> Configures the build with global properties from properties file.">
    <if><isset property="basePropertiesFile"/><then>
      <available file="${project.basedir}${php.directory_separator}${basePropertiesFile}" property="basePropertiesFileExists"/>
        <if><not><isset property="basePropertiesFileExists"/></not><then>
           <fail>Could not find the specified base properties file ${project.basedir}${php.directory_separator}${basePropertiesFile}. Stopping.</fail>
        </then>
        <else>
          <property name="build.basePropertiesFile" value="${project.basedir}${php.directory_separator}${basePropertiesFile}" />
          <echo>Using ${build.basePropertiesFile} as base properties file.</echo>
        </else>  
        </if>
       </then>
      
    <else> <!--Use default base properties file -->
      <available file="${project.basedir}${php.directory_separator}drupalorg_testing-site-base.properties" property="basePropertiesFileExists"/>
      <if><not><isset property="basePropertiesFileExists"/></not><then> 
        <warn>Could not find the default base properties file ${project.basedir}${php.directory_separator}drupalorg_testing-site-base.properties. Only the user properties file can be used.</warn>
      </then>
      <else>
        <property name="build.basePropertiesFile" value="${project.basedir}${php.directory_separator}drupalorg_testing-site-base.properties" />
        <echo>Using default base properties file ${build.basePropertiesFile}.</echo>
      </else>  
      </if>
    </else>  
    </if>
    
    <if><isset property="propertiesFile"/><then> 
      <!--Use user properties from specified file -->
      <available file="${project.basedir}${php.directory_separator}${propertiesFile}" property="propertiesFileExists"/>
      <if><not><isset property="propertiesFileExists"/></not><then>
        <fail>Could not find the specified user properties file ${project.basedir}${php.directory_separator}${propertiesFile}. Stopping.</fail>
      </then>
      <else>
        <property name="build.propertiesFile" value="${project.basedir}${php.directory_separator}${propertiesFile}" />
        <echo>Using ${build.propertiesFile} as user properties file.</echo>
      </else>
      </if>
    </then>
      
    <else> <!--Use default user properties file --> 
      <available file="${project.basedir}${php.directory_separator}drupalorg_testing-site.properties" property="propertiesFileExists"/>
      <if><not><isset property="propertiesFileExists"/></not><then> 
        <warn>Could not find the default user properties file ${project.basedir}${php.directory_separator}drupalorg_testing-site.properties. Only the base properties file will be used.</warn>
      </then>
      <else>
        <property name="build.propertiesFile" value="${project.basedir}${php.directory_separator}drupalorg_testing-site.properties" />
        <echo>Using ${build.propertiesFile} as default user properties file.</echo>      
      </else>  
      </if>  
    </else>
    </if>
      
   
    <if><and><not><isset property="build.basePropertiesFile" /></not><not><isset property="build.propertiesFile" /></not></and><then>
      <fail>Cannot use either the user or base properties files. Stopping </fail>
    </then>
    </if>
    
    <if><isset property="build.basePropertiesFile" /><then>
      <property file="${build.basePropertiesFile}" override="true"/>
    </then>
     <else><echo>No base properties file used.</echo></else>
    </if>
    
    <if><isset property="build.propertiesFile" /><then>
      <if><equals arg1="${build.propertiesFile}" arg2="${dast.home}${php.directory_separator}drupalorg_testing-site.properties" /><then>
        <if><and><isset property="build.basePropertiesFile" /><not><equals arg1="${build.basePropertiesFile}" arg2="${dast.home}${php.directory_separator}drupalorg_testing-site-base.properties" /></not></and><then>
          <echo>The base properties file ${build.basePropertiesFile} is not the default so the default user properties file will not be loaded.</echo>
        </then>
        <else>   
          <property file="${build.propertiesFile}" override="true"/>
          <echo>Using default user properties file ${dast.home}${php.directory_separator}drupalorg_testing-site.properties.</echo>
        </else>
        </if>  
      </then>
      <else><property file="${build.propertiesFile}" override="true"/></else>
      </if>
    </then>  
    <else><echo>No user properties file used.</echo></else>
    </if>
   
   <if><not><isset property="build.log.defaultdir"/></not><then>
     <fail>build.log.defaultdir property not set, check the ${propertiesFile} file for omissions or bad syntax</fail>
   </then>
   </if>
    <available file="${build.log.defaultdir}" property="build.log.defaultdirExists"/>
    <if><not><isset property="build.log.defaultdirExists"/></not><then>
     <echo>${build.log.defaultdir} doesn't exist, attempting to create...</echo>
     <mkdir dir="${build.log.defaultdir}"/>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.dir"/></not>
    <then>
     <fail>drupal.dir property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.Url"/></not>
    <then>
     <fail>drupal.Url property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    


  </target>
  
  <!-- ================================= 
          target: configure-create-files-dir              
         ================================= -->
  <target name="configure-create-files-dir" depends="configure" description="--> Configures the create-files-dir task with properties from a properties file.">
    <if><not><isset property="drupal.filesDirPath"/></not><then>
     <fail>drupal.filesDirPath property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    <if><not><isset property="drupal.filesDirPath"/></not><then>
     <fail>drupal.filesDirPath property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    <if><not><isset property="drupal.filesDirPermission"/></not><then>
     <fail>drupal.filesDirPath property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
  </target>

  
  <!-- ================================= 
          target: configure-create-database              
         ================================= -->
  <target name="configure-create-database" depends="configure" description="--> Configures the create-database target with properties from properties file.">
    <if>
     <not><isset property="drupal.database.driver"/></not>
    <then>
     <fail>drupal.database.driver property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.host"/></not>
    <then>
     <fail>drupal.database.host property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.name"/></not>
    <then>
     <fail>drupal.database.name property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.user"/></not>
    <then>
     <fail>drupal.database.user property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>

    <if>
     <not><isset property="drupal.database.user.host"/></not>
    <then>
     <fail>drupal.database.user.host property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.user.pass"/></not>
    <then>
     <fail>drupal.database.user.pass property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.CreateScript"/></not>
    <then>
     <fail>drupal.database.CreateScript property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.CreateScript.user"/></not>
    <then>
     <fail>drupal.database.CreateScript.user property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.CreateScript.user.pass"/></not>
    <then>
     <fail>drupal.database.CreateScript.user.pass property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="drupal.database.CreateScript.name"/></not>
    <then>
     <fail>drupal.database.CreateScript.name not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <property name="drupal.database.Url" value="${drupal.database.driver}://${drupal.database.user}:${drupal.database.user.pass}@${drupal.database.host}/${drupal.database.name}" />
    <property name="drupal.database.CreateUrl" value="${drupal.database.driver}://${drupal.database.CreateScript.user}:${drupal.database.CreateScript.user.pass}@${drupal.database.host}/${drupal.database.CreateScript.name}" /> 
  
  </target>

  <!-- ================================= 
          target: configure-create-apachehttpd-site              
         ================================= -->
  <target name="configure-create-apachehttpd-site" depends="configure" description="--> Configures the create-apache-httpd-site target with properties from properties file.">
    
    <if>
     <not><isset property="apachehttpd.apache2.conf"/></not>
    <then>
     <fail>apachehttpd.apache2.conf property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="apachehttpd.httpd.conf"/></not>
    <then>
     <fail>apachehttpd.httpd.conf property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="apachehttpd.ports.conf"/></not>
    <then>
     <fail>apachehttpd.ports.conf property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="apachehttpd.sites-enabled.dir"/></not>
    <then>
     <fail>apachehttpd.sites-enabled.dir property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>   
    
     <if>
       <not><isset property="apachehttpd.site.vhost.conf"/></not>
     <then>
       <fail>apachehttpd.site.vhost property not set, check the properties files for omissions or bad syntax</fail>
     </then>
     </if>
    
     <if>
      <not><isset property="apachehttpd.site.vhost"/></not>
     <then>
      <fail>apachehttpd.site.vhost property not set, check the properties files for omissions or bad syntax</fail>
     </then>
     </if>
    
    <if>
     <not><isset property="apachehttpd.site.vhost.ServerAdmin"/></not>
    <then>
     <fail>apachehttpd.site.vhost.ServerAdmin property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="apachehttpd.site.vhost.DocumentRoot"/></not>
    <then>
     <fail>apachehttpd.site.vhost.DocumentRoot property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="apachehttpd.site.vhost.ServerName"/></not>
    <then>
     <fail>apachehttpd.site.vhost.ServerName property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="apachehttpd.site.vhost.ErrorLog"/></not>
    <then>
     <fail>apachehttpd.site.vhost.ErrorLog property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
    <if>
     <not><isset property="apachehttpd.site.vhost.CustomLog"/></not>
    <then>
     <fail>apachehttpd.site.vhost.CustomLog property not set, check the properties files for omissions or bad syntax</fail>
    </then>
    </if>
    
  </target>

  <target name="dump-config" depends="configure"><DumpProperties/></target>

</project>