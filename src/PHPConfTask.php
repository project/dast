<?php
/**
 * @file
 * PHPConf task used to read/write to PHP .ini files
 * 
 * @author Allister Beharry
 * @package org.drupal.dast.tasks
 * @todo Fix overwriting directives
 */

require_once 'phing/Task.php';
include_once 'phing/tasks/system/ExecTask.php';
include_once 'phing/types/Commandline.php';
include_once 'phing/system/io/PhingFile.php';
include_once 'phing/util/FileUtils.php';
require_once 'PEAR_Config/Config.php';

class PHPConfTask extends Task {

  protected $file                      = null;    // The php.ini file to read/write to e.g /etc/php5/conf.d/mysql.ini...
  protected $confdirectives            = array(); // The new conf directives to add to the .ini file
  protected $confsections              = array(); // The new conf section directives to add to the .ini file
  protected $phpcommand                = null;    // Command which will be run after ini file has been written...can be used to start/restart app servers like memcache

  /**
   * Sets the PHP .ini file to write to
   * 
   * @param string $file
   * 
   */
  public function setFile($file) {
    $this->file = new PhingFile($file);
  }
  
  
  /**
   * Getter for File
   *
   * @return PhingFile
   */
  public function getFile() {
    return $this->file;
  } 
 
  /**
   * Nested creator, creates a PHPConfDirective
   *
   * @return  object  The created PHPConfDirective object
   */
  public function createPHPConfDirective() {
    $num = array_push($this->confdirectives, new PHPConfDirective());
    return $this->confdirectives[$num-1];
    }

  /**
   * Getter for PHPConfDirectives
   *
   * @return array containing all PHPConfDirectives in the ini file
   */
  public function getConfDirectives() {
    return $this->confdirectives;  
  }

  /**
   * Nested creator, creates an PHPConfSection
   *
   * @return PHPHttpdConfSection
   */
  public function createPHPConfSection() {
   $num = array_push($this->confsections, new PHPConfSection());
    return $this->confsections[$num-1];  
  }
  
 
  /**
   * Getter for ConfSections
   *
   * @return array containing all child PHPConfSections in conf file 
   */
  public function getConfSections() {
    return $this->confsections;
  }
  
  /**
   * Sets the command which will be run after .ini file has been written...can be used to start/restart app server
   * 
   * @param string $phpcommand
   */
  public function setPHPCommand($phpcommand) {
    $this->phpcommand = $phpcommand;
  }
  
  
  /**
   * Getter for PHPCommand
   * 
   * @returns string 
   */
  public function getPHPCommand() {
    return $this->phpcommand;
  }
  
  /**
   * The main task entry-point
   *
   * @return true on success
   * @throws BuildException
   */
  public function main() {

    $phpini = new Config(); //Create new PEAR::Config object
    $phpconf = ''; //This var will hold the contents of our PHP .ini file

    if (! $this->file->exists()) {
      if (!$this->file->createNewFile()) throw new BuildException('Could not create new file '.$this->file->getAbsolutePath());
      else 
      $this->log($this->file->getName()." does not exist; created at ".$this->file->getAbsolutePath());
    }
    $datasrc = $this->file->getName();
    $phpconf = & $phpini->parseConfig($datasrc, 'inicommented'); 
    if (PEAR::isError($phpconf)) {
      throw new BuildException($phpconf->getMessage());
    }
    //$this->log($phpconf->toString('phparray', array('name' => 'php_ini')));

    /* backup the existing .ini file*/
    $backupno = 1; 
    while (file_exists($datasrc.'.dast-bak.'.$backupno)): 
      $backupno++; 
    endwhile;
    copy($datasrc, $datasrc.'.dast-bak.'.$backupno);
    
    //$phpconfroot = & $phpconf->getItem('section', 'PHP');
    if (PEAR::isError($phpconf)) {
        throw new BuildException($phpconf->getMessage());
    }
    $phpconf->createBlank();
    if (PEAR::isError($phpconf)) throw new BuildException($phpconf->getMessage());
    
    /* Create standalone directives */
    for ($i=0; $i < count($this->confdirectives); $i++) {
      $this->log('Creating directive '.$this->confdirectives[$i]->getDirectiveName().' with value '.$this->confdirectives[$i]->getDirectiveValue().'...');
      
      /**FIXME*/
      /*Overwrite existing directive if DirectiveOverwrite*/
      if ($this->confdirectives[$i]->getDirectiveOverwrite() == 'yes') {
        if (PEAR::isError($phpconf->getItem('directive', $this->confdirectives[$i]->getDirectiveName()))) throw new BuildException($phpconf->getMessage());
        if ($phpconf->getItem('directive', $this->confdirectives[$i]->getDirectiveName())) {
          $directive = &$phpconf->getItem('directive', $this->confdirectives[$i]->getDirectiveName());
          $this->log('Existing directive '.$directive->getName().' will be overwritten with value '.$this->confdirectives[$i]->getDirectiveValue().'...', Project::MSG_WARN);
          if (PEAR::isError($directive->setContent($this->confdirectives[$i]->getDirectiveValue()))) throw new BuildException($directive->getMessage());
          if (PEAR::isError($phpconf->createComment('MODIFIED by DAST '.date("D M j G:i:s T Y"), 'before', &$directive))) throw new BuildException($phpconf->getMessage());
        }
      }
      else {
        if (PEAR::isError($phpconf)) throw new BuildException($phpconf->getMessage());
        $phpconf->createComment('Added by DAST PHPConfTask on '.date("D M j G:i:s T Y").':');
        if (PEAR::isError($phpconf)) throw new BuildException($phpconf->getMessage());
        $phpconf->createDirective($this->confdirectives[$i]->getDirectiveName(), $this->confdirectives[$i]->getDirectiveValue());
        if (PEAR::isError($phpconf)) throw new BuildException($phpconf->getMessage());
      }
       
    }//for i
        
    //$this->log($phpconf->toString('inicommented'));
    
    /* create sections with directives */
    for ($i=0; $i < count($this->confsections); $i++) {
      $this->log('Creating section '.$this->confsections[$i]->getSectionName().' ...');

      if (PEAR::isError($section = & $phpconf->createSection($this->confsections[$i]->getSectionName()))) throw new BuildException($section->getMessage());
      $sectiondirectives = $this->confsections[$i]->getDirectives();
      for ($j=0; $j < count($sectiondirectives); $j++) {
        $this->log('Creating directive '.$sectiondirectives[$j]->getDirectiveName().' with value '.$sectiondirectives[$j]->getDirectiveValue().'...');
        if (PEAR::isError($section->createComment('Added by DAST PHPConfTask on '.date("D M j G:i:s T Y").':'))) throw new BuildException($section->getMessage());
        if (PEAR::isError($section->createDirective($sectiondirectives[$j]->getDirectiveName(), $sectiondirectives[$j]->getDirectiveValue()))) throw new BuildException($section->getMessage()); 
      }
    }
    
    
    // Write configuration
    if (!PEAR::isError($write = $phpini->writeConfig())) 
      $this->log('Wrote configuration to '.$this->file->getAbsolutePath());
    else 
      throw new BuildException($write->getMessage());
    
    if ($this->phpcommand ==null) return;
    
    /* Use the ExecTask to handle execution of the HttpdCommand */        
    $exe = new ExecTask($this->project);
    $exe->setProject($this->project);      
    $exe->setCommand($this->phpcommand);
    
    $this->log($this->phpcommand, Project::MSG_INFO);
      $retCode = $exe->execute();
      $this->log("retCode=" . $retCode, Project::MSG_DEBUG);
      
      /*Throw an exception if exec exited with error*/
      if ($retCode !== 0) 
          throw new BuildException('Exec PHPCommand '.'exited with error code '. $retCode.Phing::getProperty("line.separator").
                                    'Command line was ['. $this->phpcommand.'] '.$this->getLocation()); 
  }

  
}