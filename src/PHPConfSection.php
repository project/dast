<?php
/**
 * @file
 * PHPConfSection Phing type used to declare a new or existing conf section to hold PHPConfDirectives for PHP .ini files
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Fix check for circular references 
 */

require_once 'phing/types/DataType.php';
require_once 'PHPConfDirective.php';

class PHPConfSection extends DataType {
  
  protected $sectionName          = null;    // The name of the PHP conf section e.g. [MySQL]...note that as of now PHP doesn't really use sections
  protected $directives           = array(); // The array of PHPConfDirectives this section will contain
  protected $childsections        = array(); // The array of child PHPConfSections this aection can contain
  
  /**
   * Makes this instance in effect a reference to another PHPConfSection instance.
   * You must not set another attribute or nest elements inside this element if you make it a reference.
   * 
   * @param Reference $r A reference to another defined PHPConfSection
   * @throws BuildException
   * 
   */
    public function setRefid(Reference $r) {
      if (! $this->project->getReference($r->getRefId()))
       throw new BuildException($r->getRefId().' '.'does not exist in the build project.');
      
      if ( isset($this->sectionType) || isset($this->sectionName) ) 
        throw $this->tooManyAttributes();
      
      if ((count($this->directives) != 0) || (count($this->childsections) != 0)) 
        throw $this->noChildrenAllowed();
      
        parent::setRefid($r);
    }

  /**
   * Performs the check for circular references and returns the referenced PHPConfSection.
   * 
   * @return PHPConfSection
   * @throws BuildException
   * 
   */
  public function getRef(Project $p) {
    if (!$this->checked) {
      $stk = array();
      array_push($stk, $this);
      $this->dieOnCircularReference($stk, $p);            
    }

    $o = $this->ref->getReferencedObject($p);
    if (!($o instanceof PHPConfSection)) 
      throw new BuildException($this->ref->getRefId()." doesn't denote a PHPConfSection");
    else return $o;       
  }
  
  /**
   * The name of the PHP conf section
   *
   * @param string $sectionName
   */
  public function setSectionName($sectionName) {
    if ($this->isReference()) throw $this->tooManyAttributes();
    $this->sectionName = $sectionName;
  }
  
  /**
   * Getter for SectionName
   *
   * @return string
   */
  public function getSectionName () {
    if ($this->isReference()) return $this->getRef($this->getProject())->sectionName;
    return $this->sectionName;
  }
  
/**
   * Nested creator, creates an PHPConfDirective
   *
   * @return object The created PHPConfConfDirective object
   */
  public function createPHPConfDirective() {
    if ($this->isReference()) throw $this->noChildrenAllowed();
    $num = array_push($this->directives, new PHPConfDirective());
    return $this->directives[$num-1];
    }
    
  /**
   * Getter for Directives
   *
   * @return array containing all PHPConfDirectives created in this section
   */
  public function getDirectives() {
    if ($this->isReference()) return $this->getRef($this->getProject())->directives;
    return $this->directives;  
  }
  
  
  /**
   * Nested creator, creates a child PHPConfSection
   *
   * @return PHPConfSection
   */
  public function createPHPConfSection() {
    if ($this->isReference()) throw $this->noChildrenAllowed();
    $num = array_push($this->childsections, new PHPConfSection());
    return $this->childsections[$num-1];  
  }
  
 
  /**
   * Getter for ChildSections
   *
   * @return array containing all child PHPConfSections created in this section 
   */
  public function getChildSections() {
    if ($this->isReference()) return $this->getRef($this->getProject())->childsections;
    return $this->childsections;
  }
   
}