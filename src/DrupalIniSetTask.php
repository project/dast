<?php
/**
 * @file
 * DrupalIniSetTask is a Phing task to retrieve, create, modify or delete ini_set PHP statements ( like ini_set('max_execution_time', 1600) )
 * embedded in settings.php or another .php file that will modify the PHP environment a Drupal site will run in 
 * @throws BuildException
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @see PHPConf* and ApacheHttpdConf* and MySQLConf* classes for tasks for reading/writing to PHP, Apache and
 * MySQL configuration files
 * 
 */

require_once 'phing/Task.php';

class DrupalIniSetTask extends Task {
  
    protected $directiveName          = null;   // The name of the directive e.g. session.auto_start
    protected $directiveValue         = null;   // The value of the directive e.g. 0
    protected $action                 = null;   // The action to take on the directive - get/add/overwrite/delete
    protected $file                   = null;   // The .php file to search for ini_set calls
  
    /**
   	* The name of the directive e.g. max_execution_time...
   	* 
   	* @param string directiveNme
   	*/
    public function setDirectiveName($directiveName) {
      $this->directiveName = $directiveName;
    }

    /**
   	* Getter for DirectiveName
   	* 
   	* @return string DirectiveNme
   	*/
    public function getDirectiveName() {
      return $this->directiveName;
    }

    /**
   	* The value of the directive...depending on action, this is to be used as follows:
   	*   action=get  (returns value of directive if found in $File in a property name referenced by $DirectiveValue)
   	*   action=add  (adds directive with value $DirectiveValue to $File, does not check for existing directives so multiple instances may be added)
   	*   action=overwrite (adds directive to $file, checks for existing directives and overwrites them each with $DirectiveValue)
   	*   action=delete (deletes the directive wherever found in the $File) 
   	* 
   	* @param string directiveValue
   	*/
    public function setDirectiveValue($directiveValue) {
      $this->directiveValue = $directiveValue;
    }
  
    /**
   	* Getter for DirectiveValue
   	* 
   	* @return string DirectiveValue
   	*/
    public function getDirectiveValue() {
      return $this->directiveValue;
    }
    
    
    /**
     * The action to take on the directive, can be one of get/add/overwrite/delete
     *
     * @param string $action
     */
    public function setAction($action) {
      $this->action = $action;
    }
    
    /**
   	* Getter for Action
   	* 
   	* @return string Action
   	*/
    public function getAction() {
      return $this->action;
    }
    
    /**
     * The .php file to scan for ini_set statements
     *
     * @param string $file
     */
    public function setFile($file) {
      $this->file = $file;
    }
    
    /**
     * Getter for File
     * 
     * @return string File
     *
     */
    public function getFile() {
      return $this->file;
    }
    
    
}