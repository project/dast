<?php
/**
 * @file
 * ApacheHttpdConf task used to read/write to Apache conf files
 * Currently only writes to file specified by File
 * @author Allister Beharry
 * @package org.drupal.dast.tasks
 * @todo Add n-level section handling, currently only goes 1-deep
 * @todo add SiteFile operations
 * @todo add restartcommand operations
 */

require_once 'phing/Task.php';
include_once 'phing/tasks/system/ExecTask.php';
include_once 'phing/types/Commandline.php';
include_once 'phing/system/io/PhingFile.php';
include_once 'phing/util/FileUtils.php';
require_once 'PEAR_Config/Config.php';
 
class ApacheHttpdConfTask extends Task {
  
  protected $file                      = null;    // The httpd conf file to read/write to e.g /etc/apache2/httpd.conf...
  protected $sitefile                  = null;    // Not currently used
  protected $confdirectives            = array(); // The new conf directives to add to the conf file
  protected $confsections              = array(); // The new conf section directives to add to the conf file
  protected $httpdcommand              = null;    // Command which will be run after conf file has been written...can be used to start/restart Apache httpd 
                                                  
    
  /**
   * Sets the Apache httpd conf file to write to
   * 
   * @param string $file
   * 
   */
  public function setFile($file) {
    $this->file = $file;
  }
  
  
  /**
   * Getter for File
   *
   * @return PhingFile
   */
  public function getFile() {
    return $this->file;
  }
  
  /**
   * The Apache conf file defining the new site which will be included at the end of the global file
   *
   * @param string $sitefile
   */
  public function setSiteFile($siteFile) {
    $this->siteFile = new PhingFile($siteFile);
  }
  

  /**
   * Getter for SiteFile
   *
   * @return PhingFile
   */
  public function getSiteFile() {
    return $this->siteFile;
  }
  
  
  /**
   * The command to run after the conf file has been/msg
   *
   * @param string $sitefile
   */
  public function setHttpdCommand($httpdcommand) {
    $thiS->httpdcommand = $httpdcommand;
  }
  
  /**
   * Getter for HttpdCommand
   *
   * @return string
   */
  public function getHttpdCommand() {
    return $this->httpdcommand;
  }
  
  /**
   * Nested creator, creates an ApacheHttpdConfDirective
   *
   * @return  object  The created ApacheHttpdConfDirective object
   */
  public function createApacheHttpdConfDirective() {
    $num = array_push($this->confdirectives, new ApacheHttpdConfDirective());
    return $this->confdirectives[$num-1];
    }

  /**
   * Getter for ApacheHttpdConfDirectives
   *
   * @return array containing all ApacheHttpdConfDirectives in the conf file
   */
  public function getConfDirectives() {
    return $this->confdirectives;  
  }
  
  
  /**
   * Nested creator, creates an ApacheHttpConfSection
   *
   * @return ApacheHttpdConfSection
   */
  public function createApacheHttpdConfSection() {
   $num = array_push($this->confsections, new ApacheHttpdConfSection());
    return $this->confsections[$num-1];  
  }
  
 
  /**
   * Getter for ConfSections
   *
   * @return array containing all child ApacheHttpdConfSections in conf file 
   */
  public function getConfSections() {
    return $this->confsections;
  }

  public function init () {}
  
  /**
   * The main task entry-point
   *
   * @return true on success
   * @throws BuildException
   */
  public function main() {
    
    $this->_validateAttributes();
    
    $conf = new Config(); //Create new PEAR::Config object
    $content = ''; //This var will hold the contents of our Apache conf file

    if ( !file_exists($this->file) ) {
      $handle = fopen($this->file, 'w');
      if ( !$handle ) throw new BuildException('Could not create new file '.$this->file); 
      fclose($handle);
      $this->log($this->file." does not exist; created at ".$this->file);
    }
       
    $datasrc = $this->file;
    $content = $conf->parseConfig($datasrc, 'apache');
    
    if (PEAR::isError($content)) throw new BuildException($content->getMessage());
     
    /* backup the existing conf file*/
    $backupno = 1; 
    while (file_exists($datasrc.'.dast-bak.'.$backupno)): 
      $backupno++; 
    endwhile;
    copy($datasrc, $datasrc.'.dast-bak.'.$backupno);
    
//    if (PEAR::isError($content->createBlank())) {
//        throw new BuildException($content->getMessage());
//    }
    
    /* Create directives */
    for ($i=0; $i < count($this->confdirectives); $i++) {
      $this->log('Creating directive '.$this->confdirectives[$i]->getDirectiveName().' '.$this->confdirectives[$i]->getDirectiveContent().'...');
      
      /*Skip identical directives with same content */ 
      if ($content->getItem('directive', $this->confdirectives[$i]->getDirectiveName())) {
        $directive = $directive = &$content->getItem('directive', $this->confdirectives[$i]->getDirectiveName());
        if ( trim($directive->getContent()) == trim($this->confdirectives[$i]->getDirectiveContent())) {
          $this->log('Skipping identical directive '.trim($directive->getName()).' '.trim($directive->getContent()));
        }
        /*Overwrite existing directive if DirectiveOverwrite*/
        elseif ($this->confdirectives[$i]->getDirectiveOverwrite() == 'yes') {
          $this->log('Existing directive '.$directive->getName().' will be overwritten with value '.$this->confdirectives[$i]->getDirectiveContent().'...', Project::MSG_WARN);
          if (PEAR::isError($directive->setContent($this->confdirectives[$i]->getDirectiveContent()))) throw new BuildException($directive->getMessage());
          if (PEAR::isError($content->createComment('MODIFIED BY DAST ApacheHttpdConfTask on '.date("D M j G:i:s T Y"), 'before', &$directive))) throw new BuildException($content->getMessage());
        }
        else {
          if (PEAR::isError($directive = &$content->createDirective($this->confdirectives[$i]->getDirectiveName(), $this->confdirectives[$i]->getDirectiveContent()))) 
          throw new BuildException($content->getMessage());
          if (PEAR::isError($content->createComment('ADDED BY DAST ApacheHttpdConfTask on '.date("D M j G:i:s T Y"), 'before', &$directive))) throw new BuildException($content->getMessage());
        }
      } //if ($content->getItem('directive', $this->confdirectives[$i]->getDirectiveName()))             
      else
      {
        if (PEAR::isError($directive = $content->createDirective($this->confdirectives[$i]->getDirectiveName(), $this->confdirectives[$i]->getDirectiveContent()))) 
          throw new BuildException($content->getMessage());
        if (PEAR::isError($content->createComment('ADDED BY DAST ApacheHttpdConfTask on '.date("D M j G:i:s T Y"), 'before', &$directive))) throw new BuildException($content->getMessage());
      }
    }//for i
    
      
      /*Duplicate existing section TODO: check overwrite attribute*/
//@FIXME
//      if ($content->getItem('section', $this->confsections[$i]->getSectionType(), array($this->confsections[$i]->getSectionName()))) {
//        $section = & $content->getItem('section', $this->confsections[$i]->getSectionType(), array($this->confsections[$i]->getSectionName()));
//        $this->log('Existing section '.$section->getName().' '.$section->getContent().' will be duplicated by '.$this->confsections[$i]->getSectionType().' '.$this->confsections[$i]->getSectionName().'...', Project::MSG_WARN);
//      }
        
//      $sectioncontent = &$content->createSection($this->confsections[$i]->getSectionType(), array($this->confsections[$i]->getSectionName()));
//      $sectiondirectives = & $this->confsections[$i]->getDirectives();
//      for ($j=0; $j < count($sectiondirectives); $j++) {
//        $this->log('Creating directive '.$sectiondirectives[$j]->getDirectiveName().' '.$sectiondirectives[$j]->getDirectiveContent().'...');
//        /*Overwrite existing directive if DirectiveOverwrite*/
//        // $this->log($sectiondirectives[$j]->getDirectiveName().$sectiondirectives[$j]->getDirectiveOverwrite());
//        if ($sectiondirectives[$j]->getDirectiveOverwrite()) {
//          if ($sectioncontent->getItem('directive', $sectiondirectives[$j]->getDirectiveName())) {
//            $directive = &$sectioncontent->getItem('directive', $sectiondirectives[$j]->getDirectiveName());
//            $this->log('Existing directive '.$directive->getName().' will be overwritten with value '. $sectiondirectives[$j]->getDirectiveContent().'...', Project::MSG_WARN);
//            if (PEAR::isError($directive->setContent($sectiondirectives[$j]->getDirectiveContent()))) throw new BuildException($directive->getMessage());
//            if (PEAR::isError($sectioncontent->createComment('MODIFIED DAST '.date("D M j G:i:s T Y"), 'before', &$directive))) throw new BuildException($sectioncontent->getMessage());
//          }
//        }
//        else {
//          $sectioncontent->createDirective($sectiondirectives[$j]>getDirectiveName(), $sectiondirectives[$j]->getDirectiveContent());
//          if (PEAR::isError($sectioncontent)) {
//          throw new BuildException($sectioncontent->getMessage());
//          }
//        }
//        
//      }//for j
      /*Create new section */
    /* Write sections */
    for ($i=0; $i < count($this->confsections); $i++) {
      $this->log('Creating section '.$this->confsections[$i]->getSectionType().' '.$this->confsections[$i]->getSectionName().'...');
      
      /* @FIXME: Check for duplicate section with same type and name */
      if ($content->getItem('section', $this->confsections[$i]->getSectionType().' '.$this->confsections[$i]->getSectionName()))  
        $this->log('Skipping identical section '.trim($section->getName()).' '.trim($section->getContent()));
        
      $sectioncontent = & $content->createSection($this->confsections[$i]->getSectionType(), array($this->confsections[$i]->getSectionName()));
      if (PEAR::isError($sectioncontent)) throw new BuildException($sectioncontent->getMessage());
      if (PEAR::isError($content->createComment('ADDED BY DAST ApacheHttpdConfTask on '.date("D M j G:i:s T Y"), 'before', &$sectioncontent))) throw new BuildException($content->getMessage());
      $sectiondirectives = $this->confsections[$i]->getDirectives();
      for ($j=0; $j < count($sectiondirectives); $j++) {
        $this->log('Creating directive '.$sectiondirectives[$j]->getDirectiveName().' '.$sectiondirectives[$j]->getDirectiveContent().'...');
        if (PEAR::isError($directive= &$sectioncontent->createDirective($sectiondirectives[$j]->getDirectiveName(), $sectiondirectives[$j]->getDirectiveContent())))
          throw new BuildException($sectioncontent->getMessage());
        //if (PEAR::isError($sectioncontent->createComment('ADDED BY DAST ApacheHttpdConfTask on '.date("D M j G:i:s T Y"), 'before', &$directive))) throw new BuildException($sectioncontent->getMessage());  
      } //for j    
      $childsections = $this->confsections[$i]->getChildSections();
      for ($k=0; $k < count($childsections); $k++) {
        $this->log('Creating section '.$childsections[$k]->getSectionType().' '.$childsections[$k]->getSectionName().'...');
        if (PEAR::isError($childsectioncontent = &$sectioncontent->createSection($childsections[$k]->getSectionType(), array($childsections[$k]->getSectionName()))))
          throw new BuildException($childsectioncontent->getMessage());
        if (PEAR::isError($sectioncontent->createComment('ADDED BY DAST ApacheHttpdConfTask on '.date("D M j G:i:s T Y"), 'before', &$childsectioncontent))) throw new BuildException($sectioncontent->getMessage());  
        $childsectiondirectives = $childsections[$k]->getDirectives();
        for ($l=0; $l < count($childsectiondirectives); $l++) {
          $this->log('Creating directive '.$childsectiondirectives[$l]->getDirectiveName().' '.$childsectiondirectives[$l]->getDirectiveContent().'...');
          
          if (PEAR::isError($childsectioncontent->createDirective($childsectiondirectives[$l]->getDirectiveName(), $childsectiondirectives[$l]->getDirectiveContent()))) 
            throw new BuildException($childsectioncontent->getMessage());          
       } //for l
      }//for k           
    }//for j

    //$this->log($content->toString('apache'));

    // Write configuration
    if (!PEAR::isError($write = $conf->writeConfig())) 
      $this->log('Wrote configuration to '.$this->file);
    else 
      throw new BuildException($write->getMessage());
    
    if ($this->httpdcommand ==null) return;
    
    /* Use the ExecTask to handle execution of the HttpdCommand */        
    $exe = new ExecTask($this->project);
    $exe->setProject($this->project);      
    $exe->setCommand($this->httpdcommand);
    
    $this->log($this->httpdcommand, Project::MSG_INFO);
      $retCode = $exe->execute();
      $this->log("retCode=" . $retCode, Project::MSG_DEBUG);
      
      /*Throw an exception if exec exited with error*/
      if ($retCode !== 0) 
          throw new BuildException('Exec HttpdCommand '.'exited with error code '. $retCode.Phing::getProperty("line.separator").
                                    'Command line was ['. $this->httpcommand.'] '.$this->getLocation());
               
  }
     
  /**
   * Validates attributes coming in from XML build file
   *
   * @access  private
   * @return  void
   * @throws  BuildException
   */
  private function _validateAttributes() {
    
    if ($this->file === null) {
      throw new BuildException("ApacheHttpdConfTask. You must specify the Apache httpd conf file to use - it will be created if it does not exist.");
      }

    }
}