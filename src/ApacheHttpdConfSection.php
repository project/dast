<?php
/**
 * @file
 * ApacheHttpdConfSection Phing type used to declare a new or existing conf section to hold ApacheHttpdConfDirectives and ApacheHttpdConfComments
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Fix check for circular references 
 */

require_once 'phing/types/DataType.php';
require_once 'ApacheHttpdConfDirective.php';

class ApacheHttpdConfSection extends DataType {

  protected $sectionType          = null;    // The httpd conf section type e.g. <VirtualHost>, <Directory>...
  protected $sectionName          = null;    // The name of the httpd conf section - 127.0.0.1,/var/www/site...
  protected $directives           = array(); // The array of ApacheHttpdConfDirectives this section will contain
  protected $childsections        = array(); // The array of child ApacheHttpdConfSectiona thia section can contain
  
  /**
   * Makes this instance in effect a reference to another ApacheHttpdConfSection instance.
   * You must not set another attribute or nest elements inside this element if you make it a reference.
   * 
   * @param Reference $r A reference to another defined ApacheHttpdConfSection
   * @throws BuildException
   * 
   */
    public function setRefid(Reference $r) {
      if (! $this->project->getReference($r->getRefId()))
       throw new BuildException($r->getRefId().' '.'does not exist in the build project.');
      
      if ( isset($this->sectionType) || isset($this->sectionName) ) 
        throw $this->tooManyAttributes();
      
      if ((count($this->directives) != 0) || (count($this->childsections) != 0)) 
        throw $this->noChildrenAllowed();
      
        parent::setRefid($r);
    }

  /**
   * Performs the check for circular references and returns the
   * referenced ApacheHttpdConfSection.
   * 
   * @return ApacheHttpdConfSection
   * @throws BuildException
   * 
   */
    public function getRef(Project $p) {
      if (!$this->checked) {
        $stk = array();
        array_push($stk, $this);
        $this->dieOnCircularReference($stk, $p);            
      }

      $o = $this->ref->getReferencedObject($p);
      if (!($o instanceof ApacheHttpdConfSection)) 
        throw new BuildException($this->ref->getRefId()." doesn't denote a ApacheHttpdConfSection");
       else return $o;       
    }
      
  /**
   * The httpd conf section type e.g VirtualHost, Directory
   *
   * @param string $sectionType
   *  
   */
  public function setSectionType($sectionType) {
    if ($this->isReference()) throw $this->tooManyAttributes();
    $this->sectionType = $sectionType;
  }
  
  /**
   * Getter for SectionType
   *
   * @return string
   */
  public function getSectionType() {
    if ($this->isReference()) return $this->getRef($this->getProject())->sectionType; 
    return $this->sectionType;
  }
  
  /**
   * The name of the httpd conf section
   *
   * @param string $sectionName
   */
  public function setSectionName($sectionName) {
    if ($this->isReference()) throw $this->tooManyAttributes();
    $this->sectionName = $sectionName;
  }
  
  /**
   * Getter for SectionName
   *
   * @return string
   */
  public function getSectionName () {
    if ($this->isReference()) return $this->getRef($this->getProject())->sectionName;
    return $this->sectionName;
  }
  
  
  /**
   * Nested creator, creates an ApacheHttpdConfDirective
   *
   * @return  object  The created ApacheHttpdConfDirective object
   */
  public function createApacheHttpdConfDirective() {
    if ($this->isReference()) throw $this->noChildrenAllowed();
    $num = array_push($this->directives, new ApacheHttpdConfDirective());
    return $this->directives[$num-1];
    }
    
  /**
   * Getter for Directives
   *
   * @return array containing all ApacheHttpdConfDirectives created in this section
   */
  public function getDirectives() {
    if ($this->isReference()) return $this->getRef($this->getProject())->directives;
    return $this->directives;  
  }
  
  
  /**
   * Nested creator, creates a child ApacheHttpConfSection
   *
   * @return ApacheHttpdConfSection
   */
  public function createApacheHttpdConfSection() {
    if ($this->isReference()) throw $this->noChildrenAllowed();
    $num = array_push($this->childsections, new ApacheHttpdConfSection());
    return $this->childsections[$num-1];  
  }
  
 
  /**
   * Getter for ChildSections
   *
   * @return array containing all child ApacheHttpdConfSections created in this section 
   */
  public function getChildSections() {
    if ($this->isReference()) return $this->getRef($this->getProject())->childsections;
    return $this->childsections;
  }
      
  
}
