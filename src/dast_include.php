/* Set any INI options for PHP */
ini_set('track_errors', 1); 

define('DAST_HOME', realpath(realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR));
define('DAST_TEST', DAST_HOME.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'tests');
define('DAST_TEMP', DAST_HOME.DIRECTORY_SEPARATOR.'tests');
define('PHING_TEST_BASE', DAST_HOME.DIRECTORY_SEPARATOR.'src');

if (getenv('PHP_CLASSPATH')) {
    define('PHP_CLASSPATH',  
     DAST_CLASSPATH.PATH_SEPARATOR.getenv('PHP_CLASSPATH') . PATH_SEPARATOR . get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
} else {
    define('PHP_CLASSPATH',  DAST_CLASSPATH.PATH_SEPARATOR.get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
}