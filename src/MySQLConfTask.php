<?php
/**
 * @file
 * MySQLConf task used to read/write to MySQL option .ini files
 * @author Allister Beharry
 * @package org.drupal.dast.tasks
 * @todo Add n-level section handling, currently only goes 1-deep
 * @todo add restartcommand operations
 */

require_once 'phing/Task.php';
include_once 'phing/tasks/system/ExecTask.php';
include_once 'phing/types/Commandline.php';
include_once 'phing/system/io/PhingFile.php';
include_once 'phing/util/FileUtils.php';
require_once 'PEAR_Config/Config.php';

class MySQLConfTask extends Task {

  protected $file                      = null;    // The MySQL option file to read/write to e.g /etc/mysql/my.cnf...
  protected $confsections              = array(); // The new or existing conf sections with new directives to add to the option file
  protected $mysqlcommand              = null;    // Command which will be run after option file has been written...can be used to start/restart mysqld

  /**
   * Sets the MySQL option file to write to
   * 
   * @param string $file
   * 
   */
  public function setFile($file) {
    $this->file = new PhingFile($file);
  }

  /**
   * Getter for File
   *
   * @return object PhingFile
   */
  public function getFile() {
    return $this->file;
  }
  
  
  /**
   * Nested creator, creates a MySQLConfSection
   *
   * @return object MySQLConfSection the created MySQLConfSection
   */
  public function createMySQLConfSection() {
    $num = array_push($this->confsections, new MySQLConfSection());
    return $this->confsections[$num-1];
  } 

  /**
   * Getter for MySQLConfSections
   * 
   * @retun array containing all the sections in the option file 
   */
  public function getMySQLConfSections() {
    return $this->confsections;
  }
  
  /**
   * Sets the command which will be run after option file has been written...can be used to start/restart mysqld
   * 
   * @param string $mysqlcommand
   */
  public function setMySQLCommand($mysqlcommand) {
    $this->mysqlcommand = $mysqlcommand;
  }
  
  
  /**
   * Getter for MySQLCommand
   * 
   * @returns string 
   */
  public function getMySQLCommand() {
    return $this->mysqlcommand;
  }
  
  /**
   * The main task entry-point
   *
   * @return true on success
   * @throws BuildException
   */
  public function main() {

    $mysqlini = new Config(); //Create new PEAR::Config object
    $mysqlconf = ''; //This var will hold the contents of our MySQL option file

    if (! $this->file->exists()) {
      if (!$this->file->createNewFile()) throw new BuildException('Could not create new file '.$this->file->getAbsolutePath());
      else 
      $this->log($this->file->getName()." does not exist; created at ".$this->file->getAbsolutePath());
    }
    $datasrc = $this->file->getName();
    $mysqlconf = $mysqlini->parseConfig($datasrc, 'mysqlini'); //MySQL option files are in standard PHP ini format 
    if (PEAR::isError($mysqlconf)) {
      throw new BuildException($mysqlconf->getMessage());
    }
    //$this->log($content->toString('phparray', array('name' => 'php_ini')));
    
    $content->createBlank();
    $content->createComment('BEGIN DAST MySQLConfTask on '.date("D M j G:i:s T Y"));
    
    /* Create directives */
    for ($i=0; $i < count($this->confdirectives); $i++) {
      $this->log('Creating directive '.$this->confdirectives[$i]->getDirectiveName().' '.$this->confdirectives[$i]->getDirectiveContent().'...');
      $content->createDirective($this->confdirectives[$i]->getDirectiveName(), $this->confdirectives[$i]->getDirectiveContent()); 
    }
 
  } 
  /**
   * Validates attributes coming in from XML build file
   *
   * @access  private
   * @return  void
   * @throws  BuildException
   */
  private function _validateAttributes() {
    
    if ($this->file === null) {
      throw new BuildException("MySQLConfTask. You must specify the MySQL option file to use - it will be created if it does not exist.");
      }

    }
                                                  
}