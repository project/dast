<?php
/**
 * @file
 * Simpletest unit test suite for site-install.xml build script
 * @author Allister Beharry
 * @package org.drupal.dast.base
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
include_once('creole/Creole.php');

class SiteInstallTest extends SimpleBuildFileTest {
  
  function __construct ()  {
    $this->UnitTestCase();
    define('BASE_PROPERTIES_FILE', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-install-base-test.properties');
    define('USER_PROPERTIES_FILE', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-install-test.properties');
    define('DAST_EXT', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'ext');
    $this->configureProject(PHING_TEST_BASE.DIRECTORY_SEPARATOR.'site-install.xml');
    //$this->executeTarget("main");
  }
  
  function __destruct() {
    Phing::shutdown();
  }

  public function setUp() { 

  }

  /**
   * Test that SimpleTest is working properly
   *
   */
  function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');
  }
  
  /**
   * Test that base properties are set
   *
   */
  function testBasePropertiesSet() {
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE); //Set the properties file we are using for the test
    $this->executeTarget("configure");
    $this->assertPropertyEquals('build.basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->assertPropertyEquals('build.log.defaultdir', "E:\var\log\DAST");
    $this->assertPropertyEquals('drupal.dir', 'E:\Webs\drupal52\dev\dast\test\install');
    $this->assertPropertyEquals('drupal.Url', 'http://localhost/drupal52/dev/dast/test/install/');
    $this->assertPropertyEquals('drupal.settings.source', '');
    $this->assertPropertyEquals('drupal.settings.location', 'sites\default\settings.php');
    $this->assertPropertyEquals('drupal.database.Url', 'mysql://joe:cool@localhost/DRUPAL52');
    $this->assertPropertyEquals('drupal.locale', 'en');
    $this->assertPropertyEquals('drupal.profile', 'default');
    $this->assertPropertyEquals('drupal.profile.fetch', 'no');
    
  }
  
  /**
   * Test that user properties are set
   */
  function testUserPropertiesSet() {
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE); //Set the base properties file we are using for the test
    $this->project->setProperty('propertiesFile', USER_PROPERTIES_FILE); //Set the user properties file we are using for the test
    $this->executeTarget("configure");
    $this->assertPropertyEquals('build.basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->assertPropertyEquals('build.propertiesFile', USER_PROPERTIES_FILE);
    $this->assertPropertyEquals('build.log.defaultdir', "E:\var\log\DAST");
    $this->assertPropertyEquals('drupal.dir', 'E:\Webs\drupal52\dev\dast\test\install');
    $this->assertPropertyEquals('drupal.Url', 'http://localhost/drupal52/dev/dast/test/install/');
    $this->assertPropertyEquals('drupal.settings.source', '');
    $this->assertPropertyEquals('drupal.settings.location', 'sites\default\settings.php');
    $this->assertPropertyEquals('drupal.database.Url', 'mysql://joe:cool@localhost/DRUPAL52_drupalorg_testing');
    $this->assertPropertyEquals('drupal.locale', 'en');
    $this->assertPropertyEquals('drupal.profile', 'drupalorg_testing');
    $this->assertPropertyEquals('drupal.database.Url', 'mysql://joe:cool@localhost/DRUPAL52_drupalorg_testing');
    $this->assertPropertyEquals('drupal.profile.fetch', 'yes');
    //@TODO rest of properties
     
  }
  
 
  function testSetDbUrl() {
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE); //Set the base properties file we are using for the test
    $this->project->setProperty('propertiesFile', USER_PROPERTIES_FILE); //Set the user properties file we are using for the test
    $this->executeTarget('configure');
    
    /*Create a clean drupal dir */
    if(file_exists('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php')) 
      unlink('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php');
    $this->assertFalse(file_exists('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
    
    echo ('Creating clean Drupal dir for testSetDbUrl: ');
    echo (exec('xcopy "E:\My Documents\Web Development Projects\Drupal\drupalCVS\Drupal-5-2\drupal" "E:\Webs\drupal52\dev\dast\test\install" /e /i /y', $out, $ret));
    $this->assertTrue($ret == 0);
    $this->assertTrue(file_exists('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
           
    $handle = fopen('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php', "r");
    $settings = fread($handle, filesize('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
    fclose($handle);
    unset($handle);
    $this->assertFalse(strpos($settings,'$db_url = '."'mysql://joe:cool/localhost/DRUPAL52'"));
    $this->project->setProperty('propertiesFile', NULL);
    $this->executeTarget('set-db-url');
    $handle = fopen('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php', "r");
    $settings = fread($handle, filesize('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
    fclose($handle);
    unset($handle);
    $this->assertTrue(strpos($settings,'$db_url = '."'mysql://joe:cool@localhost/DRUPAL52_drupalorg_testing'"));
   }
  
  function testfetchProfile() {
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE); //Set the base properties file we are using for the test
    $this->project->setProperty('propertiesFile', USER_PROPERTIES_FILE); //Set the user properties file we are using for the test
    $this->executeTarget("configure");
    
    /*Create a clean drupal dir */
    echo ('Creating clean Drupal dir for testfetchProfile: ');
    if (file_exists('E:\Webs\drupal52\dev\dast\test\install\profiles\drupalorg_testing\drupalorg_testing.profile')) 
      unlink('E:\Webs\drupal52\dev\dast\test\install\profiles\drupalorg_testing\drupalorg_testing.profile');    
    echo (exec('xcopy "E:\My Documents\Web Development Projects\Drupal\drupalCVS\Drupal-5-2\drupal" "E:\Webs\drupal52\dev\dast\test\install\" /e /i /y', $out, $ret));
    $this->assertTrue($ret == 0);
    $this->assertFalse(file_exists('E:\Webs\drupal52\dev\dast\test\install\profiles\drupalorg_testing\drupalorg_testing.profile'));
    $this->executeTarget('fetch-profile');
    $this->assertTrue(file_exists('E:\Webs\drupal52\dev\dast\test\install\profiles\drupalorg_testing\drupalorg_testing.profile'));
    
  }
  
  function testMain() {
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE); //Set the base properties file we are using for the test
    $this->project->setProperty('propertiesFile', USER_PROPERTIES_FILE); //Set the user properties file we are using for the test
    
    echo ('Creating clean Drupal dir for testMain: ');
    if(file_exists('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php')) 
      unlink('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php');
    $this->assertFalse(file_exists('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
    
    echo (exec('xcopy "E:\My Documents\Web Development Projects\Drupal\drupalCVS\Drupal-5-2\drupal" "E:\Webs\drupal52\dev\dast\test\install" /e /i /y', $out, $ret));
    $this->assertTrue($ret == 0);
    $this->assertTrue(file_exists('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
    
    $handle = fopen('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php', "r");
    $settings = fread($handle, filesize('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
    fclose($handle);
    unset($handle);
    $this->assertFalse(strpos($settings,'$db_url = '."'mysql://joe:cool@localhost/DRUPAL52_drupalorg_testing'"));
        
    $this->executeTarget("main");
    
    $this->assertPropertyEquals('build.basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->assertPropertyEquals('build.propertiesFile', USER_PROPERTIES_FILE);
    $this->assertPropertyEquals('build.log.defaultdir', "E:\var\log\DAST");
    $this->assertPropertyEquals('drupal.dir', 'E:\Webs\drupal52\dev\dast\test\install');
    $this->assertPropertyEquals('drupal.Url', 'http://localhost/drupal52/dev/dast/test/install/');
    $this->assertPropertyEquals('drupal.settings.source', '');
    $this->assertPropertyEquals('drupal.settings.location', 'sites\default\settings.php');
    $this->assertPropertyEquals('drupal.locale', 'en');
    $this->assertPropertyEquals('drupal.database.Url', 'mysql://joe:cool@localhost/DRUPAL52_drupalorg_testing');
    $this->assertPropertyEquals('drupal.locale', 'en');
    $this->assertPropertyEquals('drupal.profile', 'drupalorg_testing');
    $this->assertPropertyEquals('drupal.profile.fetch', 'yes');
    //@TODO Add check for rest of props

    $handle = fopen('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php', "r");
    $settings = fread($handle, filesize('E:\Webs\drupal52\dev\dast\test\install\sites\default\settings.php'));
    fclose($handle);
    unset($handle);
    $this->assertTrue(strpos($settings,'$db_url = '."'mysql://joe:cool@localhost/DRUPAL52_drupalorg_testing'"));
    
    $this->assertTrue(file_exists('E:\Webs\drupal52\dev\dast\test\install\profiles\drupalorg_testing\drupalorg_testing.profile'));

  }
   /**
   * Initialise the build project and set required properties including base properties file 
   */ 
  protected function configureProject($filename) { 
   //Init Phing project
   $this->logBuffer = "";
   $this->fullLogBuffer = "";
   $this->project = new Project();
   $this->project->init();
   $f = new PhingFile($filename);
   $this->project->setUserProperty( "phing.file" , $f->getAbsolutePath() );
   
   $this->project->setProperty('dast.ext', DAST_EXT);
   $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests  
   
   $this->project->addBuildListener(new PhingTestListener($this));
   ProjectConfigurator::configureProject($this->project, $f);
   
  }  
    
 }