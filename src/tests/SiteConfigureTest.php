<?php
/**
 * @file
 * Simpletest unit test suite for site-configure.xml build script
 * @author Allister Beharry
 * @package org.drupal.dast.base
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
include_once('creole/Creole.php');

class SiteConfigureTest extends SimpleBuildFileTest {
  
  function __construct ()  {
    $this->UnitTestCase();
    define('BASE_PROPERTIES_FILE', 'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-configure-base-test.properties');
    define('USER_PROPERTIES_FILE', 'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-configure-test.properties');
    $this->configureProject(PHING_TEST_BASE.DIRECTORY_SEPARATOR.'site-configure.xml');
    //$this->executeTarget("main");
  }
  
  function __destruct() {
    Phing::shutdown();
  }

  public function setUp() { 

  }

  /**
   * Test that SimpleTest is working properly
   *
   */
  function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');
  }
  
  /**
   * Test that base properties are set
   *
   */
  function testBasePropertiesSet() {
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->executeTarget("configure");
    $this->assertPropertyEquals('build.basePropertiesFile', PHING_TEST_BASE.DIRECTORY_SEPARATOR.BASE_PROPERTIES_FILE);
    //Test some properties set in the config file
    $this->assertPropertyEquals('build.testing', 'yes');
    $this->assertPropertyEquals('drupal.dir', 'E:\Webs\ApacheDrupal\drupal52-dev\dast\test\configure');
    $this->assertPropertyEquals('drupal.Url', 'http://localhost:8002/drupal52-dev/dast/test/configure/');

  }
  
  /**
   * Test the create-database task can copy the script file to the dast.tmp directory replacing the 
   * ${drupal.database.* tokens in the file with their property values from the build file,
   * successfully create the database and user, and then delete the temp file. 
   *
   */
  function testCreateDatabase() {
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->executeTarget('configure-create-database');
    
    /*Test database properties set*/
    $this->assertPropertyEquals('drupal.database.driver', 'mysql');
    $this->assertPropertyEquals('drupal.database.host', 'localhost');
    $this->assertPropertyEquals('drupal.database.name', 'DRUPAL52_TEST_CONFIGURE');
    $this->assertPropertyEquals('drupal.database.user', 'testconfigure');
    $this->assertPropertyEquals('drupal.database.user.host', 'localhost');
    $this->assertPropertyEquals('drupal.database.user.pass', 'testconfigure');
    $this->assertPropertyEquals('drupal.database.CreateScript', 'MYSQL5-CREATE-DRUPAL-DB.sql');
    $this->assertPropertyEquals('drupal.database.CreateScript.user', 'root');
    $this->assertPropertyEquals('drupal.database.CreateScript.user.pass', 'all!ster');
    $this->assertPropertyEquals('drupal.database.CreateScript.name', 'mysql');
    $this->assertPropertyEquals('drupal.database.Url', 'mysql://testconfigure:testconfigure@localhost/DRUPAL52_TEST_CONFIGURE');
    $this->assertPropertyEquals('drupal.database.CreateUrl', 'mysql://root:all!ster@localhost/mysql');
    
    $this->executeTarget("create-database");
    /* FIXME (where is the script?) Test the script file exists * F/
//    $this->assertTrue(file_exists(DAST_HOME.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.$this->project->getProperty("drupal.database.CreateScript")));
//    $createscriptfile = DAST_HOME.DIRECTORY_SEPARATOR.'tmp'.DIRECTORY_SEPARATOR.$this->project->getProperty("drupal.database.CreateScript");
//    $handle = fopen($createscriptfile, "r");
//    $createscript = fread($handle, filesize($createscriptfile));
//    fclose($handle);
//    unset($handle);
//   
//    /*Test token were replaced*/
//    $this->assertTrue(strpos($createscript, "DELETE FROM `tables_priv` where `User` = 'testconfigure' and 'Host' = 'localhost';") !== FALSE);
//    $this->assertTrue(strpos($createscript, "DELETE FROM `columns_priv` where `User` = 'testconfigure' and 'Host' = 'localhost';") !== FALSE);
//    $this->assertTrue(strpos($createscript, "DELETE FROM `user` where `User`= 'testconfigure' and `Host` = 'localhost'") !== FALSE);
//    $this->assertTrue(strpos($createscript, "DROP DATABASE IF EXISTS DRUPAL52_TEST_CONFIGURE;") !== FALSE);
//    $this->assertTrue(strpos($createscript, "CREATE DATABASE DRUPAL52_TEST_CONFIGURE;") !== FALSE);
//    $this->assertTrue(strpos($createscript, "GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE") !== FALSE);
//    $this->assertTrue(strpos($createscript, "TO 'testconfigure'@'localhost' IDENTIFIED BY 'testconfigure';") !== FALSE);

    $this->assertTrue($conn = Creole::getConnection($this->project->getProperty('drupal.database.Url')));
    unset($conn);
   

  }
  
  /**
   * Test the create-files-dir task can create the files directory and chmod it to the specified permission
   */
  function testCreateFilesDir() {    
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->executeTarget('configure');
    // Test properties for task are set
    $this->assertPropertyEquals('drupal.createfilesDir', 'yes');
    $this->assertPropertyEquals('drupal.filesDirPath', 'E:\Webs\ApacheDrupal\drupal52-dev\dast\test\configure\files');
    $this->assertPropertyEquals('drupal.filesDirPermission', '0777');
    
    if (file_exists($this->project->getProperty('drupal.filesDirPath')) && is_writable($this->project->getProperty('drupal.filesDirPath'))) {
      rmdir($this->project->getProperty('drupal.filesDirPath'));
    }
    else {
      echo ('Could not find find and delete dir '.$this->project->getProperty('drupal.filesDirPath'));
    }
    
    $this->executeTarget('create-files-dir');
    $this->assertTrue(file_exists($this->project->getProperty('drupal.filesDirPath')));
    // $this->assertInLogs('PHP chmod returned: 1', Project::MSG_INFO);
    //$this->assertInLogs('chmoded '.$this->project->getProperty('drupal.filesDirPath').' to '.$this->project->getProperty('drupal.filesDirPermission'));
    //@TODO Doesn't work under Windows $this->assertEqual(fileperms($this->project->getProperty('drupal.filesDirPath')), 0777);
    $this->assertTrue(is_writable($this->project->getProperty('drupal.filesDirPath')));
    //Clean up after tests
    if (file_exists($this->project->getProperty('drupal.filesDirPath')) && is_writable($this->project->getProperty('drupal.filesDirPath'))) {
      rmdir($this->project->getProperty('drupal.filesDirPath'));
    }
    else {
      echo ('Could not find find and delete dir '.$this->project->getProperty('drupal.filesDirPath'));
    }
    
  }
  
  /**
   * Test the create-apache-httpd-site can create and write to the specified conf files to create
   * the Apache httpd site 
   */
  function testCreateApacheHttpdSite() {
    /* Clean conf files if they exist */
    if (file_exists($this->project->getProperty('apachehttpd.apache2.conf'))) unlink($this->project->getProperty('apachehttpd.apache2.conf'));
    if (file_exists($this->project->getProperty('apachehttpd.httpd.conf'))) unlink($this->project->getProperty('apachehttpd.httpd.conf'));
    if (file_exists($this->project->getProperty('apachehttpd.ports.conf'))) unlink($this->project->getProperty('apachehttpd.ports.conf'));
    if (file_exists($this->project->getProperty('apachehttpd.vhost.conf'))) unlink($this->project->getProperty('apachehttpd.vhost.conf'));
    
    $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->executeTarget('create-apachehttpd-site');
    /*Validate properties configured*/
    $this->assertPropertyEquals('apachehttpd.apache2.conf', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'apache2'.DIRECTORY_SEPARATOR.'apache2.conf');
    $this->assertPropertyEquals('apachehttpd.httpd.conf', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'apache2'.DIRECTORY_SEPARATOR.'httpd.conf');
    $this->assertPropertyEquals('apachehttpd.ports.conf', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'apache2'.DIRECTORY_SEPARATOR.'ports.conf');
    $this->assertPropertyEquals('apachehttpd.sites-enabled.dir', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'apache2'.DIRECTORY_SEPARATOR.'sites-enabled');
    $this->assertPropertyEquals('apachehttpd.site.vhost.conf', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'apache2'.DIRECTORY_SEPARATOR.'sites-enabled'.DIRECTORY_SEPARATOR.'dast-test-configure.conf');
    $this->assertPropertyEquals('apachehttpd.site.vhost', '*:8002');
    $this->assertPropertyEquals('apachehttpd.site.vhost.ServerAdmin', 'allisterb@dast-test-configure.org');
    $this->assertPropertyEquals('apachehttpd.site.vhost.DocumentRoot', 'E:\Webs\ApacheDrupal\drupal52-dev\dast\test\configure');
    $this->assertPropertyEquals('apachehttpd.site.vhost.ServerName', 'dast-test-configure');
    $this->assertPropertyEquals('apachehttpd.site.vhost.ErrorLog', 'logs/dast-test-configure_log');
    $this->assertPropertyEquals('apachehttpd.site.vhost.CustomLog', 'logs/dast-test-configure-access_log common');
    
    $apacheconfsrc = $this->project->getProperty('apachehttpd.apache2.conf');
    $this->assertTrue(file_exists($apacheconfsrc));
    $conf = new Config();
    $apacheconf = &$conf->parseConfig($apacheconfsrc, 'apache');
    $this->assertFalse(PEAR::isError($apacheconf)); //Parsing file is OK
    $this->assertTrue($apacheconf->countChildren() > 0); //Config container was populated
    //echo $apacheconf->toString('apache');
    //echo $apacheconf->directiveContent('Include');
    $this->assertEqual($apacheconf->directiveContent('Include'), 'E:\My Documents\Web Development Projects\Drupal\d.o\contributions\modules\DAST\src\tests\etc\apache2\sites-enabled');
    
    $apacheconfsrc = $this->project->getProperty('apachehttpd.ports.conf');
    $this->assertTrue(file_exists($apacheconfsrc));
    $apacheconf = &$conf->parseConfig($apacheconfsrc, 'apache');
    //echo $apacheconf->toString('apache');
    $this->assertEqual($apacheconf->directiveContent('Listen'), '8002');

    $apacheconfsrc = $this->project->getProperty('apachehttpd.site.vhost.conf');
    $this->assertTrue(file_exists($apacheconfsrc));
    $apacheconf = &$conf->parseConfig($apacheconfsrc, 'apache');
    // echo $apacheconf->toString('phparray', array('name' => 'php_ini'));
    $vhost = &$apacheconf->getItem('section', 'VirtualHost');
    $this->assertFalse(PEAR::isError($vhost));
    // echo ($vhost->toString('apache'));
    //@TODO add checks for rest of vhost directives
    unset($vhost);
    unset($apacheconf);
    unset($conf);
  }
  
 
  /**
   * Initialise the build project and set required properties including base properties file 
   */
  protected function configureProject($filename) { 
   //Init Phing project
   $this->logBuffer = "";
   $this->fullLogBuffer = "";
   $this->project = new Project();
   $this->project->init();
   $f = new PhingFile($filename);
   $this->project->setUserProperty( "phing.file" , $f->getAbsolutePath() );
   $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests  

   $this->project->addBuildListener(new PhingTestListener($this));
   ProjectConfigurator::configureProject($this->project, $f);
   
  }  
    
 }