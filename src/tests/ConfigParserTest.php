<?php
/**
 * @file Simpletest test suite for parsers from PEAR::Config and PEAR:PHPParser
 * 
 * This is a testsuite and scratchpad for the different file parsers used in *Conf tasks and types 
 * @author Allister Beharry
 * @package org.drupal.dast.tasks
 * @todo Write tests using PEAR_Config to validate directives and sections are written to conf file - checked manually now 
 */

//Need to manually set DAST_HOME and include_path and init Phing if running outside Phing build file environment
if (! defined('DAST_HOME')) {
  define('DAST_HOME', 'E:\My Documents\Web Development Projects\Drupal\d.o\contributions\modules\DAST');
  define('DAST_TEST', DAST_HOME.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'tests');
  define('DAST_TEMP', DAST_HOME.DIRECTORY_SEPARATOR.'tests');
  define('PHING_TEST_BASE', 'E:\My Documents\Web Development Projects\Drupal\d.o\contributions\modules\DAST\src');
  
  define ('DAST_CLASSPATH', DAST_HOME.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'src'.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'phing'.
         DIRECTORY_SEPARATOR.'classes'.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'PHPUnit');

  if (getenv('PHP_CLASSPATH')) {
    define('PHP_CLASSPATH',  
     DAST_CLASSPATH.PATH_SEPARATOR.getenv('PHP_CLASSPATH') . PATH_SEPARATOR . get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
  } 
  else {
    define('PHP_CLASSPATH',  DAST_CLASSPATH.PATH_SEPARATOR.get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
  }
  
}

require_once('simpletest/unit_tester.php');
require_once('PEAR_Config/Config.php');
require_once('PHP/Parser.php');

class ConfigParserTest extends UnitTestCase {
  
  public function setUp () {

  }
  
  /**
   * Test that the SimpleTest is OK and that the parsers work with stock config files
   *
   */
  public function testSanity() {
    $this->assertTrue(2==2);
    echo('Echo not swallowed');

  }
  
  /**
   * Test that PEAR::Config parser and container works for stock php .ini file - the php.ini-recommended
   * file ships with all PHP 5 distros
   */
  public function testStockPHPIniParser() {
         
    $phpconfsrc = DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'php.ini-recommended';
    $this->assertTrue(file_exists($phpconfsrc)); //File is there
    
    $phpIniFile = new Config();
    $phpconf = &$phpIniFile->parseConfig($phpconfsrc, 'inicommented');
    
    $this->assertFalse(PEAR::isError($phpconf)); //Parsing file is OK
    $this->assertTrue($phpconf->countChildren() > 0); //Config container was populated
    //echo $phpconf->toString('phparray', array('name' => 'php_ini'));
    //echo $phpconf->getName();
    $phpconfroot = & $phpconf->getItem('section', 'PHP'); //All php.ini files start with this as the root section
    //echo $phpconfroot->toString('phparray', array('name' => 'php_ini'));
    $this->assertTrue($phpconfroot->countChildren() > 0); //The root item is populated
    // echo $phpconfroot->directiveContent('zend.ze1_compatibility_mode');
    $this->assertEqual($phpconfroot->directiveContent('zend.ze1_compatibility_mode'), 'Off'); //Test a stand-alone directive content can be read
    $this->assertEqual($phpconfroot->directiveContent('serialize_precision'), 100); //Test another stand-alone directive content can be read as numeric data
    
    $MySQLsection = &$phpconf-> getItem('section', 'MySQL');
    $this->assertTrue($MySQLsection->countChildren() > 0);
    //echo $MySQLsection->countChildren();
    
    //First item in MySQL section is actually a comment
    $this->assertEqual(trim($MySQLsection->getChild(0)->getContent()), 'Allow or prevent persistent links.');
    
    //Both index-based and name based addressing work
    $this->assertEqual($MySQLsection->getChild(1)->getContent(), $MySQLsection->directiveContent('mysql.allow_persistent'));
    //Directive addressing works all the way from root
    $this->assertEqual($MySQLsection->directiveContent('mysql.allow_persistent'), 'On');  
    unset($MySQLsection);
    unset($phpconf);
    unset($phpIniFile);
  }
  
  
  /**
   * Test that we can read MySQL files with the PEAR::Config MySQL ini parser - my-medium.ini a stock .ini
   * that ships with MySQL
   */
  public function testMySQLParserRead() {
    $mysqlconfsrc=DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'my-medium.ini';
    $this->assertTrue(file_exists($mysqlconfsrc), $mysqlconfsrc); //file is there
    
    $mysqlIniFile = new Config();
    $mysqlconf = & $mysqlIniFile->parseConfig($mysqlconfsrc, 'mysqlini');
    $this->assertFalse(PEAR::isError($mysqlconf));
    //echo $mysqlconf->getMessage();
    $this->assertTrue($mysqlconf->countChildren() > 0);
    //echo $mysqlconf->getChild(1)->getContent();
    //echo $mysqlconf->toString('mysqlini', array('name' => 'my_ini'));
    $mysqlclient = &$mysqlconf->getItem('section', 'client');
    //echo $mysqlclient()->getContent();
    //echo $mysqlclient->toString('phparray', array('name' => 'mysql_client'));
    $this->assertEqual($mysqlclient->directiveContent('port'), '3306');
    //echo("Dumping mysqlconf: \n");
    //var_dump($mysqlconf);
    unset($mysqlclient);
    unset($mysqlconf);
    unset($mysqlIniFile);
  }
  
  public function testMySQLParserWrite() {
    $mysqlconfsrc=DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'mysqlparsertest.ini';
    $this->assertTrue(file_exists($mysqlconfsrc), $mysqlconfsrc);
    $mysqlIniFile = new Config();
    $mysqlconf = & $mysqlIniFile->parseConfig($mysqlconfsrc, 'mysqlini');
    define('EXISTING_WRITE_SECTION', 'mysqld'); define('EXISTING_WRITE_DIRECTIVE', 'default-storage-engine'); define('EXISTING_WRITE_DIRECTIVE_VALUE', 'MyISAM');
    define('NEW_WRITE_SECTION', 'anewsection'); define('NEW_WRITE_DIRECTIVE', 'a-new-directive'); define('NEW_WRITE_DIRECTIVE_VALUE', 'A new directive value');
    $this->assertTrue($mysqlconf->getItem('section', EXISTING_WRITE_SECTION));
    $this->assertFalse($mysqlconf->getItem('section', NEW_WRITE_SECTION));
    $this->assertTrue($mysqlconf->createBlank());
    $this->assertTrue($mysqlconf->createComment ('BEGIN DAST MySQLConfTask on '.date("D M j G:i:s T Y")));
    $new_write_section = & $mysqlconf->createSection(NEW_WRITE_SECTION);
    $this->assertTrue($new_write_section);
    $this->assertTrue($mysqlconf->getItem('section', NEW_WRITE_SECTION));
    $this->assertFalse($new_write_section->getChild(0));
    $new_write_directive = & $new_write_section->createDirective(NEW_WRITE_DIRECTIVE, NEW_WRITE_DIRECTIVE_VALUE);
    $this->assertTrue($mysqlconf->createComment ('END DAST MySQLConfTask on '.date("D M j G:i:s T Y")));
    $this->assertTrue($new_write_directive);
    $this->assertEqual($new_write_directive->getContent(), NEW_WRITE_DIRECTIVE_VALUE);
    $backupno = 1; 
    while (file_exists($mysqlconfsrc.'.dast-bak.'.$backupno)): 
      $backupno++; 
    endwhile;
    $this->assertTrue(copy($mysqlconfsrc, $mysqlconfsrc.'.dast-bak.'.$backupno));
    $mysqlconf->writeDatasrc($mysqlconfsrc, 'mysqlini');
    $mysqlconf = & $mysqlIniFile->parseConfig($mysqlconfsrc, 'mysqlini');
    $this->assertTrue($mysqlconf->getItem('section', EXISTING_WRITE_SECTION));
    $this->assertTrue($mysqlconf->getItem('section', NEW_WRITE_SECTION));
    //unset($mysqlconf);
    //$this->assertTrue(unlink($mysqlconfsrc));
    //$this->assertTrue(copy($mysqlconfsrc.'.dast-bak.'.$backupno, $mysqlconfsrc));
    
  }
  
  public function testPHPParser() {
    //print_r(PHP_Parser::staticParseFile(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'settings.php'));
  }
  
 /* protected function getPEARErrorMessage($object) {
    if (PEAR::isError($object )
  }
*/  
  
}