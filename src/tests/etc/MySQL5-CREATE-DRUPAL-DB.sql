#This script drops and create a database with a user and privileges for use with a Drupal  site
#The variables joe, cool, DRUPAL52, localhost represent placeholders that will be replaced
#by literal values from the properties set in the site-configure build script.

#Revoke all privileges for the user
DELETE FROM `user` where `User` = 'joe';
DELETE FROM `tables_priv` where `User` = 'joe';
DELETE FROM `columns_priv` where `User` = 'joe';

SELECT "Deleted any existing privileges for user joe" as MSG;

#Drop the user if it exists
DELETE FROM `user` where `User`= 'joe';
SELECT "Deleted (if existed) user joe..." as MSG;

#Drop database if it exists
DROP DATABASE IF EXISTS DRUPAL52;
SELECT "Deleted (if existed) database DRUPAL52..." as MSG;

#Create database
CREATE DATABASE DRUPAL52;
SELECT "Created database DRUPAL52..." as MSG;

#Grant privileges
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE
  TEMPORARY TABLES, LOCK TABLES
ON DRUPAL52.*
TO 'joe'@'localhost' IDENTIFIED BY 'cool';
FLUSH PRIVILEGES;
SELECT "Granted privileges SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES to user joe@localhost.." as MSG;
