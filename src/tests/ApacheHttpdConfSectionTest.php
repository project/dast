<?php
/**
 * @file
 * Simpletest test suite for ApacheHttpdConfSection class
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Enable tests for circular references when fixed in ApacheHttpdConfSection.php
 * @todo Create test for invalid attributes and reference BuildExceptions using project->createDataType
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('ApacheHttpdConfSection.php');

class ApacheHttpdConfSectionTest extends SimpleBuildFileTest {
  
  /**
   * Loads the ApacheHttpdConfSectionTest.xml build file and executes target main
   *
   */
  public function __construct ()  {
    $this->UnitTestCase();
    $this->configureProject(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'types'.DIRECTORY_SEPARATOR.'ApacheHttpdConfSectionTest.xml');
    $this->executeTarget("main");
  }
  
  /**
   * Shutdown Phing when test terminates
   *
   */
  public function __destruct() {
    Phing::shutdown();
    parent::__destruct();
  }
  
  /**
   * Runs before every SimpleTest
   * 
   */
  public function setUp() { 
    
  }

  /**
   * Test that SimpleTest and our test build file are working OK
   *
   */
  public function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');

  }
  
  /**
   * Test that creating standalone confsection elements in build files is OK
   *
   */
  public function testStandaloneElementCreated () {
    $confsection = $this->project->getReference('testStandaloneElement');
    $this->assertTrue(($confsection instanceof ApacheHttpdConfSection));
  }
  
  /**
   * Test that the created stanadalone element's attributes are set properly and accessible
   * 
   */
  public function testStandAloneElementAttributesSet () {
    $confsection = $this->project->getReference('testStandaloneElement');
    $this->assertEqual($confsection->getsectionType(), 'conf1');
    $this->assertEqual($confsection->getSectionName(), 'conf1/bar1');
    
  }
  
  
  /**
   * Test that the testComplexElement confsection is created with attributes set and accessible
   *
   */
  public function testComplexElementContainerAttributesSet() {
    $confsection = $this->project->getReference('testComplexElement');
    $this->assertEqual($confsection->getsectionType(), 'VirtualHost');
    $this->assertEqual($confsection->getSectionName(), '127.0.0.1:82');
  }
  
  
  /**
   * Test that the testComplexElement confsection has the child conf section and directives set
   * 
   */
  public function testComplexElementChildDirectives() {
    $confsection = $this->project->getReference('testComplexElement');
    $this->assertEqual(count($confsection->getDirectives()), 2);
    
    /* Embedded Directives */
    $confdirectives = $confsection->getDirectives();
    $this->assertEqual($confdirectives[0]->getDirectiveName(), 'DocumentRoot');
    $this->assertEqual($confdirectives[0]->getDirectiveContent(), '/var/www/drupal6');
    $this->assertEqual($confdirectives[1]->getDirectiveName(), 'ServerName');
    $this->assertEqual($confdirectives[1]->getDirectiveContent(), 'www.foo.com');
    
    /*Child section*/
    $this->assertEqual(count($confsection->getChildSections()), 1);
    $childconfsections = $confsection->getChildSections();
    $this->assertEqual($childconfsections[0]->getSectionType(), 'Location');
    $this->assertEqual($childconfsections[0]->getSectionName(), '/admin');
    
    /* Directives in child section */
    $this->assertEqual(count($childconfsections[0]->getDirectives()), 2);
    $childconfdirectives = $childconfsections[0]->getDirectives();
    $this->assertEqual($childconfdirectives[0]->getDirectiveName(), 'AuthType');  
    $this->assertEqual($childconfdirectives[0]->getDirectiveContent(), 'basic');               
    $this->assertEqual($childconfdirectives[1]->getDirectiveName(), 'Require');  
    $this->assertEqual($childconfdirectives[1]->getDirectiveContent(), 'group admin');                  
  }
  
  /**
   * Test that referencing the testStandaloneElement works
   */
  public function testStandAloneElementReference () {
    $confsection = $this->project->getReference('testSimpleElementReference');
    $this->assertEqual($confsection->getsectionType(), 'conf1');
    $this->assertEqual($confsection->getSectionName(), 'conf1/bar1');
  }
  
  /**
   * Test that referencing the testComplexElement works
   */
  public function testComplexElementReference () {
    $confsection = $this->project->getReference('testComplexElementReference');
    $this->assertEqual(count($confsection->getDirectives()), 2);
    
    /* Embedded Directives */
    $confdirectives = $confsection->getDirectives();
    $this->assertEqual($confdirectives[0]->getDirectiveName(), 'DocumentRoot');
    $this->assertEqual($confdirectives[0]->getDirectiveContent(), '/var/www/drupal6');
    $this->assertEqual($confdirectives[1]->getDirectiveName(), 'ServerName');
    $this->assertEqual($confdirectives[1]->getDirectiveContent(), 'www.foo.com');
    
    /*Child section*/
    $this->assertEqual(count($confsection->getChildSections()), 1);
    $childconfsections = $confsection->getChildSections();
    $this->assertEqual($childconfsections[0]->getSectionType(), 'Location');
    $this->assertEqual($childconfsections[0]->getSectionName(), '/admin');
    
    /* Directives in child section */
    $this->assertEqual(count($childconfsections[0]->getDirectives()), 2);
    $childconfdirectives = $childconfsections[0]->getDirectives();
    $this->assertEqual($childconfdirectives[0]->getDirectiveName(), 'AuthType');  
    $this->assertEqual($childconfdirectives[0]->getDirectiveContent(), 'basic');               
    $this->assertEqual($childconfdirectives[1]->getDirectiveName(), 'Require');  
    $this->assertEqual($childconfdirectives[1]->getDirectiveContent(), 'group admin');
  }
  
  /**
   * Test that referencing the testStandaloneElement works
   * @todo FIXME
   */
  public function testCircularReference () {
//    $confsection = $this->project->getReference('testComplexElementCircular');
//    /*Child section*/
//    $this->assertEqual(count($confsection->getChildSections()), 1);
//    $childconfsections = $confsection->getChildSections();
//    $this->assertEqual($childconfsections[0]->getSectionType(), 'Location');
  }
  
  /**
   * configureProject overrides BuildTestFile configure Project to add properties before creating and configuring
   * the test build file.
   * @param string $filename
   * 	The name of the build file to load into the environment
   */  
  protected function configureProject($filename) { 
  /* Init Phing project */
  $this->logBuffer = "";
  $this->fullLogBuffer = "";
  $this->project = new Project();
  $this->project->init();
  $f = new PhingFile($filename);
  $this->project->setUserProperty("phing.file" , $f->getAbsolutePath());
  
  /*Set properties for test */
  $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests 
  
  /*Set base and custom properties file in test build file if needed */
  /*      
  if ( PHP_OS == 'WINNT') {
    $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test-base.properties');
    $this->project->setProperty('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test.properties');
  }
  else {
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test-base.properties');
    $this->project->Property('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test.properties');
  }
  */

 
  $this->project->addBuildListener(new PhingTestListener($this));
  ProjectConfigurator::configureProject($this->project, $f);
  }
  
}
