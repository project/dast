<?php

/**
 * @file
 * Simpletest test suite for PHPConfSection class
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Enable tests for circular references when fixed in PHPConfSection.php
 * @todo Create test for invalid attributes and reference BuildExceptions using project->createDataType
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('PHPConfSection.php');

class PHPConfSectionTest extends SimpleBuildFileTest {
  
  /**
   * Loads the ApacheHttpdConfSectionTest.xml build file and executes target main
   *
   */
  public function __construct ()  {
    $this->UnitTestCase();
    $this->configureProject(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'types'.DIRECTORY_SEPARATOR.'PHPConfSectionTest.xml');
    $this->executeTarget("main");
  }
  
  public function __destruct() {
    Phing::shutdown();
    parent::__destruct();
  }

  /**
   * Test that SimpleTest and our test build file are working OK
   *
   */
  public function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');

  }
  
  /**
   * Test that creating standalone confsection elements in build files is OK
   *
   */
  public function testStandaloneElementCreated () {
    $confsection = $this->project->getReference('testStandaloneElement');
    $this->assertTrue(($confsection instanceof PHPConfSection));
  }

  /**
   * Test that the created stanadalone element's attributes are set properly and accessible
   * 
   */
  public function testStandAloneElementAttributesSet () {
    $confsection = $this->project->getReference('testStandaloneElement');
    $this->assertEqual($confsection->getSectionName(), 'MySQL');    
  }

/**
   * Test that the testComplexElement confsection is created with attributes set and accessible
   *
   */
  public function testComplexElementContainerAttributesSet() {
    $confsection = $this->project->getReference('testComplexElement');
    $this->assertEqual($confsection->getSectionName(), 'Filter');
  }
  
  
  /**
   * Test that the testComplexElement confsection has the child directives set
   * 
   */
  public function testComplexElementChildDirectives() {
    $confsection = $this->project->getReference('testComplexElement');
    $this->assertEqual(count($confsection->getDirectives()), 1);
    
    /* Embedded Directives */
    $confdirectives = $confsection->getDirectives();
    $this->assertEqual($confdirectives[0]->getDirectiveName(), 'filter.default');
    $this->assertEqual($confdirectives[0]->getDirectiveValue(), 'unsafe_raw');
            
  }

  /**
   * Test that referencing the testStandaloneElement works
   */
  public function testStandAloneElementReference () {
    $confsection = $this->project->getReference('testSimpleElementReference');
    $this->assertEqual($confsection->getsectionName(), 'MySQL');
  }

  /**
   * Test that referencing the testComplexElement works
   */
  public function testComplexElementReference () {
    $confsection = $this->project->getReference('testComplexElementReference');
    $this->assertEqual(count($confsection->getDirectives()), 1);
    
    /* Embedded Directives */
    $confdirectives = $confsection->getDirectives();
    $this->assertEqual($confdirectives[0]->getDirectiveName(), 'filter.default');
    $this->assertEqual($confdirectives[0]->getDirectiveValue(), 'unsafe_raw');
  }
   
  protected function configureProject($filename) { 
  /* Init Phing project */
  $this->logBuffer = "";
  $this->fullLogBuffer = "";
  $this->project = new Project();
  $this->project->init();
  $f = new PhingFile($filename);
  $this->project->setUserProperty("phing.file" , $f->getAbsolutePath());
  
  /*Set properties for test */
  $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests 
 
  $this->project->addBuildListener(new PhingTestListener($this));
  ProjectConfigurator::configureProject($this->project, $f);
  }
}