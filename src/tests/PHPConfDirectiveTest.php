<?php
/**
 * @file
 * Simpletest test suite for PHPConfDirective class
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Create test for invalid attributes and reference BuildExceptions using project->createDataType
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('PHPConfDirective.php');

class PHPConfDirectiveTest extends SimpleBuildFileTest {
  /**
   * Loads the PHPConfDirectiveTest.xml build file, configures the build project and executes target main
   *
   */
  public function __construct ()  {
    $this->UnitTestCase();
    $this->configureProject(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'types'.DIRECTORY_SEPARATOR.'PHPConfDirectiveTest.xml');
    $this->executeTarget("main");
  }

  /**
   * Default destructor - shuts down Phing.
   *
   */
  function __destruct() {
    Phing::shutdown();
  }

  public function setUp() { 

  }

  /**
   * Test that SimpleTest is working properly
   *
   */
  function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');
  }

  /**
   * Test that creating standalone confsection elements in build files is OK
   *
   */
  public function testStandaloneElementCreated () {
    $confdirective = $this->project->getReference('testStandaloneElement');
    $this->assertTrue(($confdirective instanceof PHPConfDirective));
  } 

  /**
   * Test that the created stanadalone element's attributes are set properly and accessible
   * 
   */
  public function testStandAloneElementAttributesSet () {
    $confdirective = $this->project->getReference('testStandaloneElement');
    $this->assertEqual($confdirective->getDirectiveName(), 'mysql.connect_timeout');
    $this->assertEqual($confdirective->getDirectiveValue(), '60');
    $this->assertEqual($confdirective->getDirectiveLocation(), 'top');
    
  }
  
  /**
   * Test that the referenced element's attributes are set properly and accessible
   */
  public function testElementwithLocationReference () {
    $confdirective = $this->project->getReference('testElementwithLocationReference');
    $this->assertTrue($confdirective->getDirectiveLocationTarget() instanceof PHPConfDirective);
    $this->assertEqual($confdirective->getDirectiveName(), 'mysql.default_port');
    $this->assertEqual($confdirective->getDirectiveValue(), '3306');
    $this->assertEqual($confdirective->getDirectiveLocation(), 'after');
    $this->assertEqual($confdirective->getDirectiveLocationTarget()->getDirectiveName(), 'mysql.connect_timeout');
    $this->assertTrue($confdirective->getDirectiveLocationTarget() === $this->project->getReference('testStandaloneElement'));
  }
  
  /**
   * Initialise the build project and set required properties 
   */
   protected function configureProject($filename) { 
   //Init Phing project
   $this->logBuffer = "";
   $this->fullLogBuffer = "";
   $this->project = new Project();
   $this->project->init();
   $f = new PhingFile($filename);
   $this->project->setUserProperty( "phing.file" , $f->getAbsolutePath() );
   $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests  
   
   $this->project->addBuildListener(new PhingTestListener($this));
   ProjectConfigurator::configureProject($this->project, $f);
   
  }  
}