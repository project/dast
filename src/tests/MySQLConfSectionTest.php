<?php

/**
 * @file
 * Simpletest test suite for MySQLConfSection class
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Enable tests for circular references when fixed in MySQLConfSection.php
 * @todo Create test for invalid attributes and reference BuildExceptions using project->createDataType
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('MySQLConfSection.php');

class MySQLConfSectionTest extends SimpleBuildFileTest {
  
  /**
   * Loads the ApacheHttpdConfSectionTest.xml build file and executes target main
   *
   */
  public function __construct ()  {
    $this->UnitTestCase();
    $this->configureProject(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'types'.DIRECTORY_SEPARATOR.'MySQLConfSectionTest.xml');
    $this->executeTarget("main");
  }
  
  public function __destruct() {
    Phing::shutdown();
    parent::__destruct();
  }

  /**
   * Test that SimpleTest and our test build file are working OK
   *
   */
  public function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');

  }
  
  /**
   * Test that creating standalone confsection elements in build files is OK
   *
   */
  public function testStandaloneElementCreated () {
    $confsection = $this->project->getReference('testStandaloneElement');
    $this->assertTrue(($confsection instanceof MySQLConfSection));
  }

  /**
   * Test that the created stanadalone element's attributes are set properly and accessible
   * 
   */
  public function testStandAloneElementAttributesSet () {
    $confsection = $this->project->getReference('testStandaloneElement');
    $this->assertEqual($confsection->getSectionName(), 'isamchk');    
  }

/**
   * Test that the testComplexElement confsection is created with attributes set and accessible
   *
   */
  public function testComplexElementContainerAttributesSet() {
    $confsection = $this->project->getReference('testComplexElement');
    $this->assertEqual($confsection->getSectionName(), 'mysqld');
  }
  
  
  /**
   * Test that the testComplexElement confsection has the child directives set
   * 
   */
  public function testComplexElementChildDirectives() {
    $confsection = $this->project->getReference('testComplexElement');
    $this->assertEqual(count($confsection->getDirectives()), 3);
    
    /* Embedded Directives */
    $confdirectives = $confsection->getDirectives();
    $this->assertEqual($confdirectives[0]->getDirectiveName(), 'basedir');
    $this->assertEqual($confdirectives[0]->getDirectiveContent(), '/usr');
    $this->assertEqual($confdirectives[0]->getStandAloneDirective(), 'no');
    $this->assertEqual($confdirectives[1]->getDirectiveName(), 'datadir');
    $this->assertEqual($confdirectives[1]->getDirectiveContent(), '/var/lib/mysql');      
    $this->assertEqual($confdirectives[1]->getStandAloneDirective(), 'no');
    $this->assertEqual($confdirectives[2]->getDirectiveName(), 'skip-locking');
    $this->assertEqual($confdirectives[2]->getStandAloneDirective(), 'yes');             
  }

  /**
   * Test that referencing the testStandaloneElement works
   */
  public function testStandAloneElementReference () {
    $confsection = $this->project->getReference('testSimpleElementReference');
    $this->assertEqual($confsection->getsectionName(), 'isamchk');
  }

  /**
   * Test that referencing the testComplexElement works
   */
  public function testComplexElementReference () {
    $confsection = $this->project->getReference('testComplexElementReference');
    $this->assertEqual(count($confsection->getDirectives()), 3);
    
    /* Embedded Directives */
    $confdirectives = $confsection->getDirectives();
    $this->assertEqual($confdirectives[0]->getDirectiveName(), 'basedir');
    $this->assertEqual($confdirectives[0]->getDirectiveContent(), '/usr');
    $this->assertEqual($confdirectives[1]->getDirectiveName(), 'datadir');
    $this->assertEqual($confdirectives[1]->getDirectiveContent(), '/var/lib/mysql');
  }
   
  protected function configureProject($filename) { 
  /* Init Phing project */
  $this->logBuffer = "";
  $this->fullLogBuffer = "";
  $this->project = new Project();
  $this->project->init();
  $f = new PhingFile($filename);
  $this->project->setUserProperty("phing.file" , $f->getAbsolutePath());
  
  /*Set properties for test */
  $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests 
  
  /*Set base and custom properties file in test build file if needed */
  /*      
  if ( PHP_OS == 'WINNT') {
    $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test-base.properties');
    $this->project->setProperty('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test.properties');
  }
  else {
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test-base.properties');
    $this->project->Property('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test.properties');
  }
  */

 
  $this->project->addBuildListener(new PhingTestListener($this));
  ProjectConfigurator::configureProject($this->project, $f);
  }
}