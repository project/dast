<?php
/**
 * @file
 * Simpletest test suite for PHPConfTask class
 * @author Allister Beharry
 * @package org.drupal.dast.tasks 
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('PHPConfTask.php');
require_once('PEAR_Config/Config.php');

class PHPConfTaskTest extends SimpleBuildFileTest {
  
  /**
   * Loads the MySQLConfTaskTest.xml build file and executes target main
   *
   */
  public function __construct ()  {
    $this->UnitTestCase();
    $this->configureProject(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'PHPConfTaskTest.xml');
 
  }
  
  /**
   * Shutdown Phing when test terminates
   *
   */
  public function __destruct() {
    Phing::shutdown();
    parent::__construct();
  }
  
  /**
   * Runs before every SimpleTest
   * 
   */
  public function setUp() { 
    
  }

  /**
   * Test that SimpleTest and our test build file are working OK
   *
   */
  public function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');
  }
  
  
  /**
   * Test the MySQLConf task can create a new file if the File specified dpesn't exist
   *
   */
  public function testNewFileCreated() {
   $this->executeTarget("testNewFileCreated");
   $this->assertPropertySet('testNewFileCreated.fileExists');
  }
  
  /**
   * Test that the task can successfully parse an existing file
   *
   */
  public function testMain() {
    $this->executeTarget("main");
  }
  
 /**
   * configureProject overrides BuildTestFile configure Project to add properties before creating and configuring
   * the test build file.
   * @param string $filename
   * 	The name of the build file to load into the environment
   */  
  protected function configureProject($filename) { 
  /* Init Phing project */
  $this->logBuffer = "";
  $this->fullLogBuffer = "";
  $this->project = new Project();
  $this->project->init();
  $f = new PhingFile($filename);
  $this->project->setUserProperty("phing.file" , $f->getAbsolutePath());
  
  /*Set properties for test */
  $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests 
  

  $this->project->addBuildListener(new PhingTestListener($this));
  ProjectConfigurator::configureProject($this->project, $f);
  }
}