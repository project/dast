<?php
/**
 * @file
 * Simpletest test suite for site-fetch.xml build file
 * @author Allister Beharry
 * @package org.drupal.dast.base
 * @todo needs most major tests
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('util/filesystem.php');

class SiteFetchTest extends SimpleBuildFileTest {
  
  function SiteFetchTest() {
    $this->UnitTestCase();
    define('BASE_PROPERTIES_FILE', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-fetch-base-test.properties');
    define('USER_PROPERTIES_FILE', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-fetch-test.properties');
    $this->configureProject(PHING_TEST_BASE.DIRECTORY_SEPARATOR.'site-fetch.xml');
    $this->executeTarget("configure");
  }

  public function setUp() { 
    //$this->project->log($this->project->properties); 
  }

  /**
   * Test that SimpleTest is working properly
   *
   */
  function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    //$this->assertTrue(PHING_TEST_BASE =='E:\My Documents\Web Development Projects\Drupal\d.o\contributions\modules\DAST\src\tests');
   $this->assertPropertyEquals('build.testing', 'yes');
  }
  
  /**
   * Test that base and user properties are set
   *
   */
  function testPropertiesFileSet() {
   $this->executeTarget('configure');
   $this->assertPropertyEquals('build.basePropertiesFile', BASE_PROPERTIES_FILE);
   $this->assertPropertyEquals('build.propertiesFile', USER_PROPERTIES_FILE);
  }
  
  /**
   * Test that the clean target can drop and recreate the drupal.dir directory 
   *
   */
  /*function testCleanTarget() {
    $drupal_dir = $this->project->getProperty("drupal.dir");
    if (file_exists($drupal_dir)) {
      rmdirr($drupal_dir);
    }
    mkdir($drupal.dir);
    copydirr(PHING_TEST_BASE.DIRECTORY_SEPARATOR.'site-fetch.xml',$toDir,$chmod=0757,$verbose=false);
    
  }*/

/**
 * configureProject overrides BuildTestFile configure Project to add properties before creating and configuring
 * the test build file.
 * @param string $filename
 */  
protected function configureProject($filename) { 
  //Init Phing project
  $this->logBuffer = "";
  $this->fullLogBuffer = "";
  $this->project = new Project();
  $this->project->init();
  $f = new PhingFile($filename);
  $this->project->setUserProperty( "phing.file" , $f->getAbsolutePath() );
  $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests  
  $this->project->setProperty('propertiesFile', USER_PROPERTIES_FILE); //Set the user properties file we are using for the test
  $this->project->setProperty('basePropertiesFile', BASE_PROPERTIES_FILE); //Set the base properties file we are using for the test
  $this->project->addBuildListener(new PhingTestListener($this));
  ProjectConfigurator::configureProject($this->project, $f);
  }

}