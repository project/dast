<?php
//Id$
/**
 * @file Bootstrap code for running tests outside DAST/Phing build environment
 */ 

//Need to manually set DAST_HOME and include_path and init Phing if running outside Phing build file environment
if (! defined('DAST_HOME')) {
  define('DAST_HOME', 'E:\My Documents\Web Development Projects\Drupal\d.o\contributions\modules\DAST');
  define('DAST_TEST', DAST_HOME.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'tests');
  //define('DAST_TEMP', DAST_HOME.DIRECTORY_SEPARATOR.'tests');
  define('PHING_TEST_BASE', 'E:\My Documents\Web Development Projects\Drupal\d.o\contributions\modules\DAST\src');
  
  define ('DAST_CLASSPATH', DAST_HOME.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'src'.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'phing'.
         DIRECTORY_SEPARATOR.'classes'.PATH_SEPARATOR.DAST_HOME.DIRECTORY_SEPARATOR.'PHPUnit');

  if (getenv('PHP_CLASSPATH')) {
    define('PHP_CLASSPATH',  
     DAST_CLASSPATH.PATH_SEPARATOR.getenv('PHP_CLASSPATH') . PATH_SEPARATOR . get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
  } 
  else {
    define('PHP_CLASSPATH',  DAST_CLASSPATH.PATH_SEPARATOR.get_include_path());
    ini_set('include_path', PHP_CLASSPATH);
  }

 // echo "Using PHP_CLASSPATH: ".PHP_CLASSPATH."\n";


//ini_set('include_path', DAST_CLASSPATH);

  require_once 'phing/Phing.php';

  /* Setup Phing environment */
  Phing::startup();

  /* Set DAST home directory */
  Phing::setProperty('dast.home', DAST_HOME);
  
  /* Set DAST tmp directory */
  Phing::setProperty('dast.tmp', DAST_HOME.DIRECTORY_SEPARATOR.'tmp');
  
    /* Set DAST ext directory */
  Phing::setProperty('dast.ext', DAST_HOME.DIRECTORY_SEPARATOR.'ext');

  /* Set Phing home directory */
  Phing::setProperty('phing.home', DAST_HOME.DIRECTORY_SEPARATOR.'phing');
  
  /* Set Phing test base directory for our source code unit tests*/
  Phing::setProperty('phing.test.base', DAST_HOME.DIRECTORY_SEPARATOR.'src');

  /*Set PHP DIRECTORY_SEPARATOR property - needed for any filesystem ops */
  Phing::setProperty('php.directory_separator', DIRECTORY_SEPARATOR);
}