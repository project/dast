<?php
/**
 * @file
 * Simpletest test suite for MySQLConfDirective class
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Create test for invalid attributes and reference BuildExceptions using project->createDataType
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('MySQLConfDirective.php');

class MySQLConfDirectiveTest extends SimpleBuildFileTest {
  /**
   * Loads the MySQLConfDirectiveTest.xml build file and executes target main
   *
   */
  public function __construct ()  {
    $this->UnitTestCase();
    $this->configureProject(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'types'.DIRECTORY_SEPARATOR.'MySQLConfDirectiveTest.xml');
    $this->executeTarget("main");
  }
  
  /**
   * Shutdown Phing when test terminates
   *
   */
  public function __destruct() {
    Phing::shutdown();
    parent::__destruct();
  }

  /**
   * Runs before every SimpleTest
   * 
   */
  public function setUp() { 
    
  }

  /**
   * Test that SimpleTest and our test build file are working OK
   *
   */
  public function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');

  }

  /**
   * Test that creating standalone confsection elements in build files is OK
   *
   */
  public function testStandaloneElementCreated () {
    $confdirective = $this->project->getReference('testStandaloneElement');
    $this->assertTrue(($confdirective instanceof MySQLConfDirective));
  }

  /**
   * Test that the created stanadalone element's attributes are set properly and accessible
   * 
   */
  public function testStandAloneElementAttributesSet () {
    $confdirective = $this->project->getReference('testStandaloneElement');
    $this->assertEqual($confdirective->getDirectiveName(), 'default-character-set');
    $this->assertEqual($confdirective->getDirectiveContent(), 'utf-8');
    $this->assertEqual($confdirective->getDirectiveLocation(), 'top');
    $this->assertEqual($confdirective->getStandAloneDirective(), 'no'); //This attribute is false by default
    
  } 
 
  /**
   * Test that the referenced element's attributes are set properly and accessible
   */  
  public function testElementwithLocationReference () {
    $confdirective = $this->project->getReference('testElementwithLocationReference');
    $this->assertTrue($confdirective->getDirectiveLocationTarget() instanceof MySQLConfDirective);
    $this->assertEqual($confdirective->getDirectiveName(), 'port');
    $this->assertEqual($confdirective->getDirectiveContent(), '3306');
    $this->assertEqual($confdirective->getDirectiveLocation(), 'before');
    $this->assertEqual($confdirective->getDirectiveLocationTarget()->getDirectiveName(), 'default-character-set');
    $this->assertTrue($confdirective->getDirectiveLocationTarget() === $this->project->getReference('testStandaloneElement'));
  }
  
  public function testElementWithStandAloneDirective() {
    $confdirective = $this->project->getReference('testElementWithStandAloneDirective');
    $this->assertTrue($confdirective->getDirectiveName(), 'skip-locking');
    $this->assertEqual($confdirective->getStandAloneDirective(), 'yes');
  }
  
  /**
   * configureProject overrides BuildTestFile configureProject to add properties before creating and configuring
   * the test build file.
   * @param string $filename
   * 	The name of the build file to load into the environment
   */  
  protected function configureProject($filename) { 
  /* Init Phing project */
  $this->logBuffer = "";
  $this->fullLogBuffer = "";
  $this->project = new Project();
  $this->project->init();
  $f = new PhingFile($filename);
  $this->project->setUserProperty("phing.file" , $f->getAbsolutePath());
  
  /*Set properties for test */
  $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests 
  
  /*Set base and custom properties file in test build file if needed */
  /*      
  if ( PHP_OS == 'WINNT') {
    $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test-base.properties');
    $this->project->setProperty('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test.properties');
  }
  else {
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test-base.properties');
    $this->project->Property('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test.properties');
  }
  */

 
  $this->project->addBuildListener(new PhingTestListener($this));
  ProjectConfigurator::configureProject($this->project, $f);
  }
  
}