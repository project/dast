<?php
/**
 * @file
 * Simpletest test suite for ApacheHttpdConfTask class
 * @author Allister Beharry
 * @package org.drupal.dast.tasks
 * @todo Write tests using PEAR_Config to validate directives and sections are written to conf file - checked manually now (sync with MySQLConfTaskTest 
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');
require_once('ApacheHttpdConfTask.php');
require_once('PEAR_Config/Config.php');

class ApacheHttpdConfTaskTest extends SimpleBuildFileTest {
  
  /**
   * Loads the ApacheHttpdConfTaskTest.xml build file and executes target main
   *
   */
  public function __construct ()  {
    $this->UnitTestCase();
    $this->configureProject(DAST_TEST.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'tasks'.DIRECTORY_SEPARATOR.'ApacheHttpdConfTaskTest.xml');
    $this->executeTarget("main");
  }

  /**
   * Shutdown Phing when test terminates
   *
   */
  public function __destruct() {
    Phing::shutdown();
    parent::__construct();
  }
  
  /**
   * Runs before every SimpleTest
   * 
   */
  public function setUp() { 
    
  }

  /**
   * Test that SimpleTest and our test build file are working OK
   *
   */
  public function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');

  }

/**
   * configureProject overrides BuildTestFile configure Project to add properties before creating and configuring
   * the test build file.
   * @param string $filename
   * 	The name of the build file to load into the environment
   */  
  protected function configureProject($filename) { 
  /* Init Phing project */
  $this->logBuffer = "";
  $this->fullLogBuffer = "";
  $this->project = new Project();
  $this->project->init();
  $f = new PhingFile($filename);
  $this->project->setUserProperty("phing.file" , $f->getAbsolutePath());
  
  /*Set properties for test */
  $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests 
  
  /*Set base and custom properties file in test build file if needed */
  /*      
  if ( PHP_OS == 'WINNT') {
    $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test-base.properties');
    $this->project->setProperty('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-windows-test.properties');
  }
  else {
    $this->project->setProperty('build.basePropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test-base.properties');
    $this->project->Property('build.PropertiesFile', DAST_TEST.DIRECTORY_SEPARATOR.'site-fetch-nix-test.properties');
  }
  */
  $this->project->addBuildListener(new PhingTestListener($this));
  ProjectConfigurator::configureProject($this->project, $f);
  }

}