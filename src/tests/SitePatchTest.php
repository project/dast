<?php
/**
 * @file
 * Simpletest unit test suite for site-patch.xml build script
 * @author Allister Beharry
 * @package org.drupal.dast.base
 */

require_once('dast_test_init.php');
require_once('SimpleBuildFileTest.php');

class SiteFetchTest extends SimpleBuildFileTest {
  
  /**
   * Default constructor - defines constants pointing to the build and properties files and calls configureProject to init
   * the build project.
   *
   */
  function __construct ()  {
    $this->UnitTestCase();
    define('BASE_PROPERTIES_FILE', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-patch-base-test.properties');
    //define('USER_PROPERTIES_FILE', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'site-patch-base-test.properties');
    define('DAST_EXT', PHING_TEST_BASE.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'ext');
    $this->configureProject(PHING_TEST_BASE.DIRECTORY_SEPARATOR.'site-patch.xml');
    //$this->executeTarget("main");
  }
  
  /**
   * Default destructor - shuts down Phing.
   *
   */
  function __destruct() {
    Phing::shutdown();
  }

  public function setUp() { 

  }

  /**
   * Test that SimpleTest is working properly
   *
   */
  function testSanity() {
    $this->assertTrue(2==2, "assertTrue OK.");
    $this->assertPropertyEquals('build.testing', 'yes');
  }
  
  
  /**
   * Test that base properties are set
   *
   */
  function testBasePropertiesSet() {
    //echo BASE_PROPERTIES_FILE;
    //echo $this->project->getProperty( 'dast.ext.patch');
    $this->executeTarget("configure");
    $this->assertPropertyEquals('build.basePropertiesFile', BASE_PROPERTIES_FILE);
    $this->assertPropertyEquals('build.testing', 'yes');
    $this->assertPropertyEquals('dast.ext.patch', 'E:\My Documents\Web Development Projects\Drupal\d.o\contributions\modules\DAST\src\tests\ext\patch');
    $this->assertPropertyEquals('patch.method', 'HTTP');
    $this->assertPropertyEquals('patch.file', '151394-2.patch');
    $this->assertPropertyEquals('patch.options', 'p0');
    $this->assertPropertyEquals('patch.Url', 'http://drupal.org/files/issues/151394-2.patch');
    $this->assertPropertyEquals('patch.file.path', DAST_HOME.DIRECTORY_SEPARATOR.'tests'.DIRECTORY_SEPARATOR.'etc'.DIRECTORY_SEPARATOR.'151394-2.patch');
    $this->assertPropertyEquals('drupal.dir', 'E:\Webs\drupal-510\dev\dast\test\patch');
  }
  
  function testfetchPatch() {
    if(file_exists(DAST_EXT.DIRECTORY_SEPARATOR.'151394-2.patch')) {
      unlink(DAST_EXT.DIRECTORY_SEPARATOR.'151394-2.patch');
    }
    $this->assertFalse(file_exists(DAST_EXT.DIRECTORY_SEPARATOR.'151394-2.patch'));
    $this->executeTarget("fetch-patch");
    $this->assertTrue(file_exists(DAST_EXT.DIRECTORY_SEPARATOR.'patch'.DIRECTORY_SEPARATOR.'151394-2.patch'));
  }
  
  function testExecPatch () {
    /*Create a clean drupal dir */
    if (file_exists('E:\Webs\drupal-510\dev\dast\test\patch\includes\common.inc.rej')) unlink('E:\Webs\drupal-510\dev\dast\test\patch\includes\common.inc.rej');    
    echo (exec('xcopy "E:\My Documents\Web Development Projects\Drupal\drupalCVS\Drupal-5-1\drupal" "E:\Webs\drupal-510\dev\dast\test\patch" /e /i /y', $out, $ret));
    $this->assertTrue($ret == 0);
    $this->assertFalse(file_exists('E:\Webs\drupal-510\dev\dast\test\patch\includes\common.inc.rej'));
    echo $this->project->getProperty( 'dast.ext.patch');
    $this->executeTarget('exec-patch');
    $this->assertTrue(file_exists('E:\Webs\drupal-510\dev\dast\test\patch\includes\common.inc.rej'));
  }
  
  function testMain () {
    
    if(file_exists(DAST_EXT.DIRECTORY_SEPARATOR.'151394-2.patch')) {
      unlink(DAST_EXT.DIRECTORY_SEPARATOR.'151394-2.patch');
    }
    /*Create a clean drupal dir */
    if (file_exists('E:\Webs\drupal-510\dev\dast\test\patch\includes\common.inc.rej')) unlink('E:\Webs\drupal-510\dev\dast\test\patch\includes\common.inc.rej');    
    echo (exec('xcopy "E:\My Documents\Web Development Projects\Drupal\drupalCVS\Drupal-5-1\drupal" "E:\Webs\drupal-510\dev\dast\test\patch" /e /i /y', $out, $ret));
    $this->assertTrue($ret == 0);
    $this->assertFalse(file_exists('E:\Webs\drupal-510\dev\dast\test\patch\includes\common.inc.rej'));
    
    $this->executeTarget("main");
  }

  
//  function testWindowsSampleProperties() {
//    
//  }
    
  /**
   * Initialise the build project and set required properties including base properties file 
   */
  protected function configureProject($filename) { 
   //Init Phing project
   $this->logBuffer = "";
   $this->fullLogBuffer = "";
   $this->project = new Project();
   $this->project->init();
   $f = new PhingFile($filename);
   $this->project->setUserProperty( "phing.file" , $f->getAbsolutePath() );
   
   $this->project->setProperty('dast.ext', DAST_EXT);
   $this->project->setProperty('build.testing', 'yes'); //Inform the buld file we are running tests  
   $this->project->setProperty('build.basePropertiesFile', BASE_PROPERTIES_FILE); //Set the properties file we are using for the test
   
   $this->project->addBuildListener(new PhingTestListener($this));
   ProjectConfigurator::configureProject($this->project, $f);
   
  }
}