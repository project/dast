<?php
/**
 * @file
 * PHPConfDirective Phing type used to declare a new conf directive inside a PHP .ini file
 * @throws BuildException
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * 
 */

require_once 'phing/types/DataType.php';

class PHPConfDirective extends DataType {
  protected $directiveName          = null;   // The name of the directive e.g. max_execution_time, memory_limit...
  protected $directiveValue       = null;   // The content of the directive e.g. 30, 128M
  protected $directiveLocation      = null;   // The location of the directive - top, bottom, before, after
  protected $directiveLocationTarget= null;   // If locatiom is specified, then this is directive the location is relative to
  protected $directiveOverwrite     = 'no';   // Indicate if any existing directive with the same name should be overwritten

  /**
   * The name of the directive e.g. max_execution_time, memory limit...
   * 
   * @param string directiveNme
   */
  public function setDirectiveName ($directiveName) {
    $this->directiveName = $directiveName; 
  }
  
  /**
   * Getter for DirectiveName
   *
   * @return string containing directive name
   */
  public function getDirectiveName () {
    return $this->directiveName;
  }
  
  /**
   * The content of the directive
   *
   * @param string $directiveValue
   */
  public function setDirectiveValue ($directiveValue) {
    $this->directiveValue = $directiveValue;
  }
  
  /**
   * Getter for DirectiveValue
   *
   * @return string
   */
  public function getDirectiveValue () {
    return $this->directiveValue;
  }
  
  /**
   * (Optional) The location of the directive relatuve to DirectiveLocationTarget
   *
   * @param string $directiveLocation
   */
  public function setDirectiveLocation ($directiveLocation) {
    $this->directiveLocation = $directiveLocation;
  }
  
  /**
   * Getter for DirectiveLocation
   *
   * @return string
   */
  public function getDirectiveLocation() {
    return $this->directiveLocation;
  }
  
  
  /**
   * (Optional) The directive DirectiveLocation is relative to (before, after)
   *
   * @param string $directiveLocationTarget
   */
  public function setDirectiveLocationTarget($directiveLocationTarget) {
    /*Check that target directive reference exists and is of the correct type*/
    $targetdirective = $this->project->getReference($directiveLocationTarget);
    if (!($this->directiveLocation == 'before') && !($this->directiveLocation == 'after')) throw new BuildException('The DirectiveLocationTarget attribute is only used when DirectiveLocation is set to before or after.');
    if (! isset($targetdirective)) throw new BuildException('The PHPConfDirective with id '.$directiveLocationTarget.' Does not exist or has not been previously defined in the build project.');
    if (! ($targetdirective instanceof PHPConfDirective)) throw new BuildException('Element '.$directiveLocationTarget.' is not of type ApacheHttpdConfDirective.');
    $this->directiveLocationTarget = $targetdirective;
  }
  
  
  /**
   * Getter for DirectiveLocationTarget
   *
   * @return string 
   */
  public function getDirectiveLocationTarget () {
    return $this->directiveLocationTarget;
  }
  
  public function setDirectiveOverwrite($directiveOverwrite) {
    $this->directiveOverwrite = $directiveOverwrite;
  }
  
  public function getDirectiveOverwrite() {
    return $this->directiveOverwrite;
  }

  
  /**
   * Template method being called when the data type has been 
   * parsed completely.
   * @return void
   */
//  function parsingComplete() {
//    if (! isset($this->directiveName)) {
//      throw new BuildException($this->directiveLocation.'The DirectiveName attribute is required in a PHPConfDirective element.');
//    }
//  }

  
}