<?php
/**
 * @file
 * MySQLConfSection Phing type used to declare a new or existing conf section to hold MySQLConfDirectives and MySQLConfComments
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Fix check for circular references 
 */

require_once 'phing/types/DataType.php';
require_once 'MySQLConfDirective.php';

class MySQLConfSection extends DataType {
  
  protected $sectionName          = null;    // The name of the MySQL conf section e.g. [mysqld] [client]...
  protected $directives           = array(); // The array of MySQLConfDirectives this section will contain
  protected $childsections        = array(); // The array of child MySQLConfSections this aection can contain
  
  /**
   * Makes this instance in effect a reference to another MySQLConfSection instance.
   * You must not set another attribute or nest elements inside this element if you make it a reference.
   * 
   * @param Reference $r A reference to another defined MySQLConfSection
   * @throws BuildException
   * 
   */
    public function setRefid(Reference $r) {
      if (! $this->project->getReference($r->getRefId()))
       throw new BuildException($r->getRefId().' '.'does not exist in the build project.');
      
      if ( isset($this->sectionType) || isset($this->sectionName) ) 
        throw $this->tooManyAttributes();
      
      if ((count($this->directives) != 0) || (count($this->childsections) != 0)) 
        throw $this->noChildrenAllowed();
      
        parent::setRefid($r);
    }

  /**
   * Performs the check for circular references and returns the referenced MySQLConfSection.
   * 
   * @return MySQLConfSection
   * @throws BuildException
   * 
   */
  public function getRef(Project $p) {
    if (!$this->checked) {
      $stk = array();
      array_push($stk, $this);
      $this->dieOnCircularReference($stk, $p);            
    }

    $o = $this->ref->getReferencedObject($p);
    if (!($o instanceof MySQLConfSection)) 
      throw new BuildException($this->ref->getRefId()." doesn't denote a MySQLConfSection");
    else return $o;       
  }
  
  /**
   * The name of the MySQL conf section
   *
   * @param string $sectionName
   */
  public function setSectionName($sectionName) {
    if ($this->isReference()) throw $this->tooManyAttributes();
    $this->sectionName = $sectionName;
  }
  
  /**
   * Getter for SectionName
   *
   * @return string
   */
  public function getSectionName () {
    if ($this->isReference()) return $this->getRef($this->getProject())->sectionName;
    return $this->sectionName;
  }
  
/**
   * Nested creator, creates an MySQLConfDirective
   *
   * @return object The created MySQLConfConfDirective object
   */
  public function createMySQLConfDirective() {
    if ($this->isReference()) throw $this->noChildrenAllowed();
    $num = array_push($this->directives, new MySQLConfDirective());
    return $this->directives[$num-1];
    }
    
  /**
   * Getter for Directives
   *
   * @return array containing all MySQLConfDirectives created in this section
   */
  public function getDirectives() {
    if ($this->isReference()) return $this->getRef($this->getProject())->directives;
    return $this->directives;  
  }
  
  
  /**
   * Nested creator, creates a child MySQLConfSection
   *
   * @return MySQLConfSection
   */
  public function createMySQLConfSection() {
    if ($this->isReference()) throw $this->noChildrenAllowed();
    $num = array_push($this->childsections, new MySQLConfSection());
    return $this->childsections[$num-1];  
  }
  
 
  /**
   * Getter for ChildSections
   *
   * @return array containing all child MySQLConfSections created in this section 
   */
  public function getChildSections() {
    if ($this->isReference()) return $this->getRef($this->getProject())->childsections;
    return $this->childsections;
  }
   
}