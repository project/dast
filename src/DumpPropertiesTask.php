<?php
/**
 * @file
 * DumpPropertiesTask is a simple Phing task to dump all the properties defined in the current build file 
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * 
 */

require_once 'phing/Task.php';
require_once 'phing/Phing.php';
require_once 'phing/Project.php';

class DumpPropertiesTask extends Task {
  
  public function main() {
    $project = $this->project;
    $project_name = $project->getName();
    $properties = array();
    $properties = Phing::getProperties();
    foreach ($properties as $k => $v) {
      $this->log("PHING::$k = $v"); //Print Phing defined properties
    }
    
    $properties = $project->getProperties();
      foreach ($properties as $k => $v) {
        if (stripos($k, 'env.')=== FALSE) $this->log("$project_name::".$k." = $v"); //Print build-file defined properties
    }
  }
}
