<?php
/**
 * @file
 * MySQLConfDirective Phing type used to declare a directive inside a MySQL .ini option file, either standalone or inside a MySQLConfSection [section]
 * @throws BuildException
 * @author Allister Beharry
 * @package org.drupal.dast.types
 * @todo Add an overwrite attribute to overwrite existiing attributes
 */

require_once 'phing/types/DataType.php';

class MySQLConfDirective extends DataType {
  
  protected $directiveName          = null;   // The name of the directive e.g. datadir, default-storage-engine...
  protected $directiveContent       = null;   // The content of the directive
  protected $directiveLocation      = null;   // The location of the directive - top, bottom, before, after
  protected $directiveLocationTarget= null;   // If locatiom is specified, then this is the directive the location is relative to
  protected $standalonedirective    = 'no'; //This flags the directive as a stand-alone directive, e.g skip-locking

  /**
   * The name of the directive e.g. datadir, default-storage-engine...
   * 
   * @param string directiveNme
   */
  public function setDirectiveName ($directiveName) {
    $this->directiveName = $directiveName; 
  }
  
  /**
   * Getter for DirectiveName
   *
   * @return string containing directive name
   */
  public function getDirectiveName () {
    return $this->directiveName;
  }
  
  /**
   * The content of the directive
   *
   * @param string $directiveValue
   */
  public function setDirectiveContent ($directiveContent) {
    $this->directiveContent = $directiveContent;
  }
  
  /**
   * Getter for DirectiveContent
   *
   * @return string
   */
  public function getDirectiveContent () {
    return $this->directiveContent;
  }
  
  /**
   * (Optional) The location of the directive relatuve to DirectiveLocationTarget
   *
   * @param string $directiveLocation
   */
  public function setDirectiveLocation ($directiveLocation) {
    $this->directiveLocation = $directiveLocation;
  }
  
  /**
   * Getter for DirectiveLocation
   *
   * @return string
   */
  public function getDirectiveLocation() {
    return $this->directiveLocation;
  }
  
  
  /**
   * (Optional) The directive DirectiveLocation is relative to (before, after)
   *
   * @param string $directiveLocationTarget
   */
  public function setDirectiveLocationTarget($directiveLocationTarget) {
    /*Check that target directive reference exists and is of the correct type*/
    $targetdirective = $this->project->getReference($directiveLocationTarget);
    if (!($this->directiveLocation == 'before') && !($this->directiveLocation == 'after')) throw new BuildException('The DirectiveLocationTarget attribute is only used when DirectiveLocation is set to before or after.');
    if (! isset($targetdirective)) throw new BuildException('The MySQLConfDirective with id '.$directiveLocationTarget.' Does not exist or has not been previously defined in the build project.');
    if (! ($targetdirective instanceof MySQLConfDirective)) throw new BuildException('Element '.$directiveLocationTarget.' is not of type MySQLConfDirective.');
    $this->directiveLocationTarget = $targetdirective;
  }
  
  
  /**
   * Getter for DirectiveLocationTarget
   *
   * @return string 
   */
  public function getDirectiveLocationTarget () {
    return $this->directiveLocationTarget;
  }
  
  /**
   * (Optional) Flags the directive as stand-alone, defaults to false. if this is set then DirectiveContent
   * doesn't have to be specified
   *
   * @param string $standalonedirective
   */
  public function setStandAloneDirective($standalonedirective) {
    $this->standalonedirective = $standalonedirective;
  }
  
  /**
   * Getter for StandAloneDirective
   * 
   * @return string
   */
  public function getStandAloneDirective() {
    return $this->standalonedirective;
  }
  
}