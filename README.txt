
Drupal Automated Staging Toolkit README
---------------------------------------

Current Version
---------------
5.x-2.1


Release Notes
-------------
This is the fourth public release of DAST-the second after 5.x-2.0-soc07final.
It contains major improvements to the formatting and content of
documentation files like this, two new quickstart HowTos, and a new
command-line option - p which simplifies the process of running build
from the command-line. Just set -p to a directory "myproject" and DAST will
load from myproject.xml and myproject-base.properties and myproject-properties
from the my project directory. An important change to the way DAST works is that
property files are now scanned for relative to the project directory, instead
of relative to DAST_HOME. See CHANGELOG.txt for a complete rundown of bug-fixes
and new features.

A PDF snapshot of the current documentation has been included in the docs 
folder. The core of the toolkit for fetching, configuring, installiing, and 
patching a Drupal site has been completed. The four core build-files slated for
the 2.0 release are complete: site-fetch, site-configure, site-patch, and
site-install; and ready to be used by DAST end-users. All the required Phing 
tasks and types used in these core build files are now complete and ready for 
use by DAST developers. The new Phing types for reading and writing directives 
and sections to Apache .conf, PHP ini and MySQL .ini configuration files are 
complete. See the documentation home page for an understanding of the design 
and implementation of this toolkit: 
http://docs.google.com/View?docid=dcgv33zb_25d26w5n.


About
-----
DAST is a set of build projects, tasks and other extensions created on the
Phing(http://www.phing.info) framework for automating a wide variety of Drupal
testing and staging scenarios. The goal of DAST is to provide a formal,
repeatable, reusable process using an existing build framework for 'building' a
Drupal site using a formal build tool and build language. DAST is targeted at
Drupal testers and developers who must repeatably setup a Drupal site using 
different configurations and code revisions for profile and module development 
and testing, patch testing,and configuration stability testing and benchmarking. 
But DAST can also be used by Drupal end-users - for example an installation 
profile developer can bundle a DAST script with her profile so that the user of 
the profile can have her Drupal site setup and bootstrapped with a minimum of 
effort - this would be a lot easier than the current manual and error-prone 
process of bringing up a Drupal site from scratch before running the profile.

If you're tired of the download/untar/copy/drop/create database routine and you
want to automate testing patches and modules then you're in the right place. If
you're looking for a tool for Drupal build management and to do stuff like
automated unit testing and and anything that can be done to make
Drupal's devs life easier and improve code quality, you're also in the right
place. See the documentation page
at http://docs.google.com/View?docid=dcgv33zb_25d26w5n for a complete guide to
the motivation, concepts, implementation, and features of the project.


Dependencies
------------
0. PHP 5.2.x - DAST is a CLI app for *nix/Windows that requires PHP 5.2.x or 
higher , it does not run on anything lower than PHP 5.2.0. DAST also requires
PHP-PEAR to be installed for the current PHP version.

1. For the forseeable future DAST will be released with all PHP/PEAR 
library dependencies bundled in a single package. The dependencies package can
be obtained from http://www.abeharry.info/stuff/dast-deps.tar.gz or
http://www.abeharry.info/stuff/dast-deps.zip.
The current dependencies are:
 -Phing 2.3.0dev  Phing (www.phing.info) is the excellent PHPized Ant build
  framework, capable of deploying just about any PHP application using concepts
  and syntax identical to Ant.
 -Creole 1.1.0    Creole (creole.phpdb.org) is an excellent OO database
  abstraction library for PHP by the same folks who developed Phing.
 -Jargon          Jargon (creole.phpdb.org/trac/wiki/Documentation/JargonGuide)
  is a high-level library built on Creole for performing common db data
  operations.
 -SimpleTest      SimpleTest (http://simpletest.sourceforge.net) is a
  popular testing framework for PHP applications. Much of the existing unit
  testing in Drupal including the simpletestauto module uses SimpleTest.
 -PHPUnit         Another unit-testing framework that can be used - the
  SimpleTest functionality in Phing has some dependencies on this.
 -PEAR::Config    An excellent PEAR library by Bertrand Mansion
  <bmansion@mamasam.com> for reading and writing to
  different config file formats including Apache con fand PHP .ini formats.
 -PHP-PEAR       The version of PHP-PEAR which ships with PHP 5 is
  required; some bundled packages have dependencies on the base PEAR packages.

Since DAST can and does modify the code in these libraries, in the interests
of sanity it bundles its own private
versions of these libraries (except base PEAR) in its own dependencies
package. It does not touch any other versions of these libraries you have
installed with PEAR or otherwise. During startup the shell wrapper explictly
puts DAST's own libraries ahead of any others on the PHP include_path. However,
since these dependencies are bona fide PEAR packages, you need to have at least
the bare-bones PEAR system available.

2. WinNT > 4 if on Windows, any version of NT4/2000/XP should work. I will test
DAST on Vista as soon as I get the opportunity.

3. External tools - DAST and Phing build scripts may require the presence of 
external command-line tools normally present on a development environment. DAST
currently requires cvs, wget, and patch available on the command line. 
Windows users can download a version of wget for  Windows at
http://www.abeharry.info/stuff/dast-ext-tools/wget.exe.


Directory Layout
----------------
After installation (see INSTALL.txt) your directory structure will look like
this:
DAST_HOME - wherever you unpack the DAST package on your filesystem
|--bin This folder contains all the shell wrappers and executable PHP CLI 
       scripts
|--creole Contains the Creole library
|--doc User guide and API documentation
|--ext External scripts and tools used by build files, like patch scripts
|--jargon Contains the Jargon library
|--log Contains build file logs
|--projects You can use this folder for your build file projects. Contains the 
            default-site, do_testing-site and other quickstart projects.
|--phing Contains the Phing framework
|--PEAR_Config Contains the PEAR::Config library
|--simpletest Contains the SimpleTest library
|--PHPUnit Contains the PHPUnit library
|--src This folder contains all DAST-specific PHP class files and other source
       files
|--tmp DAST temporary files directory


Installing
--------------
See INSTALL.txt


Using
-----
1. Set your properties for the build. Each .xml build file has an associated 
properties file with the extension ".properties" - e.g. the do_testing-site.xml
build file in the projects/do_testing-site sub-directory has the properties
which control the build process defined in the file do_testing-site.properties.
Properties are simple name/value pairs with comments delimited by #. By
convention all required properties are stated in a comment block at the start
of the properties file. In the DAST_HOME root are sample properties files for
*nix and Windows for each of the 4 core build files. These contain all the
properties that are likely to be used in a build project. If you want to use
one of these core build files directly make a copy of one of these properties
files according to your OS in the DAST root directory and and modify it with
your settings to configure the build. But normally you will be working with
build projects, located in the projects sub-directory. Build projects have 
their own properties files and call the core build files which inherit the
properties defined in the project's properties file. You specify a properties
file for a build file mybuild.xml by naming it mybuild.properties, or by using
the -D command-line parameter, see 2 on how to specify properties files on the
command line.


2. Run your build script - To run the build file projects/do_testing-site.xml
run the folowing command from DAST_HOME:

      ./bin/dast -f ./projects/do_testing-site/do_testing-site.xml

./bin/dast is the shell wrapper which kicks off the PHP CLI script. The 
f parameter denotes the location of the build file you want to run. You
should always prepend a relative path specifier (e.g for current directory -./)
to your build file names. By default the properties file in your build file's
base dir (in this case ./project/do_testing-site) will be used to load
properties from. If you want to use another properties file, specify it using
the -D parameter:

./bin/dast -f ...<myfile>.xml -D"propertiesFile=myprops.properties"

You can also use the -p parameter which indicates a project name. Projects
are simply build files together with their properties files which reside in
their own directory under the DAST_HOME/projects directory. If you have a 
myproject.xml build file and you put it in the directory 
DAST_HOME/projects/myproject together with myproject.properties and/or
myproject-base.properties you can then run:

            ./bin/dast -p myproject
This is equivalent to
            ./bin/dast -f ./projects/myproject -D"propertiesFile=
            ./projects/myproject/myproject.properties" -D"basePropertiesFile=
            ./projects/myproject/myproject-base.properties"

The -D parameter is actually a general parameter that denotes properties you 
wish to pass in to the build process. The 'propertiesFile' property denotes the
location of the .properties file to load all the remaining properties to
use in the build process. Any properties defined in the properties can be
overriden using -D"<property>=<value>". So suppose you wanted to run the
do_testing-site build using mysite1.proerties as the properties file,
the drupal.dir as /var/www/site1 and drupal.core.method = tar you could do:

./bin/dast -f ./projects/do_testing-site/do_testing-site.xml
-D"propertiesFile=mysite1.properties" -D"drupal.dir=/var/www/site1"
-D"drupal.core.method=tar"

Properties files may be split into a "base" properties file and a "user"
properties file. You define a base properties
file for your-build.xml by  either passing it as the command-line property 
basePropetiesFile(D"basePropertiesFile=..") or naming it your-build-
base.properties. You may then create a normal properties file, called here a
user properties file as described above. What will  happen is
that when the build runs it will load both properties files. Properties not 
defined in the user properties file will be inherited from the base properties
files. Properties defined in the user properties file will override those
defined in the base properties files. In this way you may keep a properties file
containg common settings for several builds and create smaller more specific
properties files to customize each individual build. Note that specifying the -D
parameter for a base properties file will prevent the default user properties
file from being loaded...i.e for yourbuild.xml if you use
-D"basePropertiesFile=<anotheyourbuild-base.properties>" then DAST will
not look for yourbuild.properties as the default user properties file. 
You must explicitly specify your user properties file if not using the default
base properties file name.

You may also specify you want to log all output to a file using the -logfile 
<logfile> parameter.
   
All unqualified file locations and relative paths passed as parameters are
assumed relative to the DAST_HOME directory, regardless of what the current
directory is when you run the build command. If you want to use build files or
property files or other files outside DAST_HOME, use a fully qualified name.

3. Check the status of the build when the script completes - Phing logs 
extensive status information to the screen; check the results of each task in
the build to see if it completed successfully or if any errors were reported.
DAST also logs extensive info to the log dir set in build.log.defaultdir
(default /var/log/DAST or E:\var\log) so if you encounter any errors be sure to
check these logs.

4. That's it, you're done. You now have a complete working Drupal site for
development or testing or deployment. For the default profile you only have to 
create the admin account next. Profiles like drupalorg_testing will do this
automatically.



Command-line Args
-----------------
 -p <project_name> Execute the project_name.xml build file in
    DAST_HOME/projects/project_name with user properties file
    project_name.properties and base properties file
    project_name-base.properties. This is the recommended way to use DAST.
 -logfile <file>        use given file for log
 -f -buildfile <file>   use given buildfile
 -D<property>=<value>   use value for given property

Phing uses the following additional command-line argumets:
 -h -help               print this message
 -l -list               list available targets in this project
 -v -version            print the version information and exit
 -q -quiet              be extra quiet
 -verbose               be extra verbose
 -debug                 print debugging information
 -logger <classname>    the class which is to perform logging
 -find <file>           search for buildfile towards the root of the 
                        filesystem and use it


Colour codes
------------
Under *nix the Phing ANSI logger is used which allows the use of colour-coded 
text messages. Phing builds always provide detailed logging to the screen as the 
build progresses; the following is a guide to the different colours used in the 
screen log text:
 -Blue OK, informational messages echoed to the screen, also 'verbose' output 
       from task operations
 -Green OK, informational messages indicating which tasks are executing
 -Cyan(lightish Blue) OK, informational messages as task operations execute
 -Magneta(pink) Not so OK, a warning message emitted from a task operation
 -Red Bad, A task declared the build failed for some reason or an unexpected 
      PHP exception or system error was raised. Check the message details and
      verify that the relavent properties are set correctly - ensure properties
      like database Urls and the CVSROOT variables are in a valid format.


Known Issues
------------
1. Under Windows certain versions of wget (my version at least - MINGWport) may
ignore any part of the Url after the ampersand even when escaped.
wget  "http://localhost/drupal5/dev/dast/index.php?profile=default&locale=en"
only fetches http://localhost/drupal5/dev/dast/index.php?profile=default This
means that you still need to go through the 'Choose locale' part. This isn't how
wget is supposed to behave (if the Url is quoted) so I have to confirm if this
is just an eccentricity of the version I have on my system.

2. Task log files paths can't contain spaces all - if say you have DAST 
installed in e:\Drupal projects\dast then setting a task log file to point at 
.\logs\dast-patch-install-wget.log won't work because the path will expand to 
e:\Drupal projects\dast. This doesn't apply to the Phing log file which you set 
using the -logfile parameter.

3. If you use the create-files-dir task, which creates the files directory under 
drupal.dir and chmods it to 0777, the tasks works as expected but cleaning the 
site after will fail because the .htaccess Drupal drops in there is 
readonly and the build file will not be able to delete it. The default-site and
do_testing-site projects come with a task call clean-files with which you can 
sudo run to remove the files directory cleanly before the main build so you 
could go
             sudo ./bin/dast -p default-site clean-files
             ./bin/dast -p default-site
The next version I will try to figure out a way to delete the .htaccess file 
(suggestions are welcome.) For many testing and dev scenarios you may not need
to use the files directory though.

Docs
----
1. The main documentation page is on Google Docs at http://docs.google.com/Doc?id=dcgv33zb_25d26w5n. 
2. See also the Phing user manual in DAST_HOME/phing/docs/phing_guide and also the Diving into Phing...series of
articles in the SoC2007 group on the Drupal website -
http://groups.drupal.org/soc-2007 and the DAST wiki page @ 
http://groups.drupal.org/node/3913
