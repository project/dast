<?xml version="1.0" encoding="UTF-8"?>
<!-- <![CDATA[ 
]]> -->

<!-- ==================================================================================================== -->
<!--  @file This is the base site-install task in DAST. It runs the Drupal web installer for the specified-->
<!--  locale and profile. Profiles can be fetched automatically from CVS.                                 -->                                   
<!--  after the build                                                                                     -->                  
<!--  @author Allister Beharry                                                                            -->
<!--  @package org.drupal.dast.tasks                                                                      -->
<!-- ==================================================================================================== -->

<project name="site-install" default="main">  
  <taskdef name="CreoleSQLExec" classname="phing.tasks.ext.CreoleSQLExecTask"/>
  <taskdef name="CVS" classname="phing.tasks.system.CVSTask"/>
  <taskdef name="warn" classname="phing.tasks.system.WarnTask"/>
  <taskdef name="DumpProperties" classname="DumpPropertiesTask" />
  
  <!-- ================================= 
          target: set-db-url              
         ================================= -->
  <target name="set-db-url" depends="configure" description="--> Points the $db_url variable in the site's settings.php file to the drupal.database.Url specified in the build">
    
    <!--Make a backup of the settings file if it exists-->
    <available file="${drupal.dir}${php.directory_separator}${drupal.settings.location}" property="drupal.settings.location.fileExists" />
    <if><isset property="drupal.settings.location.fileExists" /><then>
      <echo>Backing up existing ${drupal.dir}${php.directory_separator}${drupal.settings.location} file</echo>
      <copy file="${drupal.dir}${php.directory_separator}${drupal.settings.location}" tofile="${drupal.dir}${php.directory_separator}${drupal.settings.location}.dast.bak" overwrite="true" />
    </then>
    </if>
    
    <!--Copy the settings from source if specified -->
    <if><isset property="drupal.settings.source" /> <then>   
      <copy file="${drupal.dir}${php.directory_separator}${drupal.settings.source}" tofile="${drupal.dir}${php.directory_separator}${drupal.settings.location}" overwrite="true" />
    </then>  
    </if>
    
    <!--Bailout if we can't find a settings.php-->
      <available file="${drupal.dir}${php.directory_separator}${drupal.settings.location}" property="drupal.settings.location.fileExistsAgain" />
      <if><not><isset property="drupal.settings.location.fileExistsAgain" /></not><then>  
        <fail>Could not find Drupal settings file ${drupal.settings.location}</fail>
      </then>
      </if>
    

    <reflexive file="${drupal.dir}${php.directory_separator}${drupal.settings.location}">
      <filterchain><replaceregexp> 
        <regexp pattern = "\$db_url = 'mysql:\/\/username:password\@localhost\/databasename';" replace="\$db_url = '${drupal.database.Url}';" ignoreCase="true"/>
        </replaceregexp>
      </filterchain>
   </reflexive>
    
  </target>
  
  
  <!-- ================================= 
          target: run-installer              
         ================================= -->
  <target name="run-installer" depends="configure" description="--> Performs any token replacement on the specified profile profile and does a GET on drupal.url/index.php">
    <!--Verify profile exists if not default-->
    <if><not><equals arg1="${drupal.profile}" arg2="default"/></not><then>
      <available file="${drupal.dir}${php.directory_separator}${drupal.profiles.path}${php.directory_separator}${drupal.profile}" type="dir" property="profileExists" />
      <if><not><isset property="profileExists" /></not><then>
        <fail> The profile ${drupal.dir}${php.directory_separator}${drupal.profiles.path}${php.directory_separator}${drupal.profile} could not be found.</fail>
      </then>
      </if>
    </then>
    </if>
    
    <!--Create a files directory if it doesn't exist - new requirement for Drupal 5.3 + -->
    <available file="${drupal.dir}${php.directory_separator}files" type="dir" property="filedirExists" />
    <if><not><isset property="filedirExists" /></not><then>
     <mkdir dir="${drupal.dir}${php.directory_separator}files" /></then>
    </if>
    
    <echo>Calling ${drupal.Url}install.php?profile=${drupal.profile}&amp;locale=${drupal.locale}</echo>
    <exec command="wget &quot;${drupal.Url}install.php?profile=${drupal.profile}&amp;locale=${drupal.locale}&quot;" 
      Output="${build.log.defaultdir}${php.directory_separator}site-install-run-installer-output.log" Error="${build.log.defaultdir}${php.directory_separator}site-install-run-installer-error.log" 
      dir="${dast.tmp}${php.directory_separator}install" escape="false" passthru="false" checkreturn="false"/>
    <!--chmod settings.php to be readonly after install-->
    <Chmod Mode="0444" file="${drupal.dir}${php.directory_separator}${drupal.settings.location}" />
  </target>
  
  
  <!-- ================================= 
          target: fetch-profile              
         ================================= -->
  <target name="fetch-profile" depends="configure" description="--> Downloads a profile from CVS to be used by the installer.">
    <echo>-------------------------------------------------</echo>
    <echo> +++++ Running site-install.xml fetch-profile +++++    </echo>
    <echo>-------------------------------------------------</echo> 
    <!--Check for required CVS properties-->
    <if><not><isset property="drupal.profiles.Cvs.CvsRoot"/></not>
      <then>
        <fail>drupal.profiles.Cvs.CvsRoot property not set, check the properties files files for omissions or bad syntax</fail>
      </then>
      </if>
      <if><not><isset property="drupal.profiles.Cvs.Command"/></not>
      <then>
        <fail>drupal.profiles.Cvs.Command property not set, check the properties files files for omissions or bad syntax</fail>
      </then>
      </if>
      <if><not><isset property="drupal.profiles.Cvs.Revision"/></not>
      <then>
        <fail>drupal.profiles.Cvs.Revision property not set, check the properties files files for omissions or bad syntax</fail>
      </then>
      </if>
      <if><not><isset property="drupal.profiles.Cvs.Path"/></not>
      <then>
        <fail>drupal.profiles.Cvs.Path property not set, check the properties files files for omissions or bad syntax</fail>
      </then>
      </if>
      
      <!--Set optional CVS properties with defaults if they aren't set-->
      <if><not><isset property="drupal.profiles.Cvs.Port"/></not>
      <then>
        <property name="drupal.profiles.Cvs.Port" value="2401"/>
        <echo>drupal.profiles.Cvs.Port property not set, using default value 2401.</echo>
      </then>
      </if>
      <if><not><isset property="drupal.profiles.Cvs.CompressionLevel"/></not>
      <then>
        <property name="drupal.profiles.Cvs.CompressionLevel" value="6"/>
        <echo>drupal.profiles.Cvs.CompressionLevel property not set, using default value 6.</echo>
      </then>
      </if>
      <if><not><isset property="drupal.profiles.Cvs.Quiet"/></not>
      <then>
        <property name="drupal.profiles.Cvs.Quiet" value="false"/>
        <echo>drupal.profiles.Cvs.Quiet property not set, using default value false.</echo>
      </then>
      </if>
      <if><not><isset property="drupal.profiles.Cvs.FailOnError"/></not>
      <then>
        <property name="drupal.profiles.Cvs.FailOnError" value="true"/>
        <echo>drupal.profiles.Cvs.FailOnError property not set, using default value true.</echo>
      </then>
      </if>
      <if><not><isset property="drupal.profiles.Cvs.Output"/></not>
      <then>
         <property name="drupal.profiles.Cvs.Output" value="${build.log.defaultdir}${php.directory_separator}site-fetch-cvs-profile-output.log"/>
         <echo>drupal.profiles.Cvs.Output property not set, using default value ${build.log.defaultdir}${php.directory_separator}site-install-cvs-fetch-profile-output.log.</echo>
       </then>
       </if>    
      <if><not><isset property="drupal.profiles.Cvs.Error"/></not>
      <then>
        <property name="drupal.profiles.Cvs.Error" value="${build.log.defaultdir}${php.directory_separator}site-install-cvs-profile-error.log"/>
        <echo>drupal.profiles.Cvs.Error property not set, using default value ${build.log.defaultdir}${php.directory_separator}site-install-cvs-fetch-profile-error.log.</echo>
      </then>
      </if>
      
      <!--Run CVS operation-->
    <echo>Executing CVS command to fetch profile ${drupal.profile}...see ${drupal.profiles.Cvs.Output} and ${drupal.profiles.Cvs.Error} for details when completed.</echo>
    <CVS CvsRoot="${drupal.profiles.Cvs.CvsRoot}" Port="${drupal.profiles.Cvs.Port}" Command="${drupal.profiles.Cvs.Command}" CompressionLevel="${drupal.profiles.Cvs.CompressionLevel}" 
      Quiet="${drupal.profiles.Cvs.Quiet}" FailOnError="${drupal.profiles.Cvs.FailOnError}" 
      Module="${drupal.profile}" ModulePath="${drupal.profiles.Cvs.Path}" Tag="${drupal.profiles.Cvs.Revision}" Dest="${drupal.dir}${php.directory_separator}${drupal.profiles.path}" 
      Output="${drupal.profiles.Cvs.Output}-${drupal.profile}.log" Error="${drupal.profiles.Cvs.Error}-${drupal.profile}.log"/>       
  </target>

  
  <!-- ================================= 
          target: main              
         ================================= -->
  <target name="main" depends="configure" description="--> Runs set-db-Url and run-installer in that sequence">
    <phingcall target="set-db-url" />
    <if><equals arg1="${drupal.profile.fetch}" arg2="yes" /><then>
      <phingcall target="fetch-profile" />
    </then>
    </if>  
    <phingcall target="run-installer" /> 
  </target>

  <!-- ================================= 
          target: configure              
         ================================= -->
  <target name="configure" description="--> Configures the build with properties from properties file.">
    <if><isset property="basePropertiesFile"/><then>
      <available file="${project.basedir}${php.directory_separator}${basePropertiesFile}" property="basePropertiesFileExists"/>
        <if><not><isset property="basePropertiesFileExists"/></not><then>
           <fail>Could not find the specified base properties file ${project.basedir}${php.directory_separator}${basePropertiesFile}. Stopping.</fail>
        </then>
        <else>
          <property name="build.basePropertiesFile" value="${project.basedir}${php.directory_separator}${basePropertiesFile}" />
          <echo>Using ${build.basePropertiesFile} as base properties file.</echo>
        </else>  
        </if>
       </then>
      
    <else> <!--Use default base properties file -->
      <available file="${project.basedir}${php.directory_separator}drupalorg_testing-site-base.properties" property="basePropertiesFileExists"/>
      <if><not><isset property="basePropertiesFileExists"/></not><then> 
        <warn>Could not find the default base properties file ${project.basedir}${php.directory_separator}drupalorg_testing-site-base.properties. Only the user properties file can be used.</warn>
      </then>
      <else>
        <property name="build.basePropertiesFile" value="${project.basedir}${php.directory_separator}drupalorg_testing-site-base.properties" />
        <echo>Using default base properties file ${build.basePropertiesFile}.</echo>
      </else>  
      </if>
    </else>  
    </if>
    
    <if><isset property="propertiesFile"/><then> 
      <!--Use user properties from specified file -->
      <available file="${project.basedir}${php.directory_separator}${propertiesFile}" property="propertiesFileExists"/>
      <if><not><isset property="propertiesFileExists"/></not><then>
        <fail>Could not find the specified user properties file ${project.basedir}${php.directory_separator}${propertiesFile}. Stopping.</fail>
      </then>
      <else>
        <property name="build.propertiesFile" value="${project.basedir}${php.directory_separator}${propertiesFile}" />
        <echo>Using ${build.propertiesFile} as user properties file.</echo>
      </else>
      </if>
    </then>
      
    <else> <!--Use default user properties file --> 
      <available file="${project.basedir}${php.directory_separator}drupalorg_testing-site.properties" property="propertiesFileExists"/>
      <if><not><isset property="propertiesFileExists"/></not><then> 
        <warn>Could not find the default user properties file ${project.basedir}${php.directory_separator}drupalorg_testing-site.properties. Only the base properties file will be used.</warn>
      </then>
      <else>
        <property name="build.propertiesFile" value="${project.basedir}${php.directory_separator}drupalorg_testing-site.properties" />
        <echo>Using ${build.propertiesFile} as default user properties file.</echo>      
      </else>  
      </if>  
    </else>
    </if>
      
   
    <if><and><not><isset property="build.basePropertiesFile" /></not><not><isset property="build.propertiesFile" /></not></and><then>
      <fail>Cannot use either the user or base properties files. Stopping </fail>
    </then>
    </if>
    
    <if><isset property="build.basePropertiesFile" /><then>
      <property file="${build.basePropertiesFile}" override="true"/>
    </then>
     <else><echo>No base properties file used.</echo></else>
    </if>
    
    <if><isset property="build.propertiesFile" /><then>
      <if><equals arg1="${build.propertiesFile}" arg2="${dast.home}${php.directory_separator}drupalorg_testing-site.properties" /><then>
        <if><and><isset property="build.basePropertiesFile" /><not><equals arg1="${build.basePropertiesFile}" arg2="${dast.home}${php.directory_separator}drupalorg_testing-site-base.properties" /></not></and><then>
          <echo>The base properties file ${build.basePropertiesFile} is not the default so the default user properties file will not be loaded.</echo>
        </then>
        <else>   
          <property file="${build.propertiesFile}" override="true"/>
          <echo>Using default user properties file ${dast.home}${php.directory_separator}drupalorg_testing-site.properties.</echo>
        </else>
        </if>  
      </then>
      <else><property file="${build.propertiesFile}" override="true"/></else>
      </if>
    </then>  
    <else><echo>No user properties file used.</echo></else>
    </if>
     
     <if><not><isset property="build.log.defaultdir"/></not><then>
       <fail>build.log.defaultdir property not set, check the ${propertiesFile} file for omissions or bad syntax</fail>
     </then>
     </if>
      <available file="${build.log.defaultdir}" property="build.log.defaultdirExists"/>
      <if><not><isset property="build.log.defaultdirExists"/></not><then>
       <echo>${build.log.defaultdir} doesn't exist, attempting to create...</echo>
       <mkdir dir="${build.log.defaultdir}"/>
      </then>
      </if>
      
      <if>
       <not><isset property="drupal.dir"/></not>
      <then>
       <fail>drupal.dir property not set, check the properties files for omissions or bad syntax</fail>
      </then>
      </if>
      
      <if>
       <not><isset property="drupal.Url"/></not>
      <then>
       <fail>drupal.Url property not set, check the properties files for omissions or bad syntax</fail>
      </then>
      </if>

      <if>
       <not><isset property="drupal.locale"/></not>
      <then>
       <fail>drupal.locale property not set, check the properties files for omissions or bad syntax</fail>
      </then>
      </if>
      
      <if>
       <not><isset property="drupal.profile"/></not>
      <then>
       <fail>drupal.profile property not set, check the properties files for omissions or bad syntax</fail>
      </then>
      </if>
      
      <if><not><isset property="drupal.profile.fetch"/></not><then>
       <fail>drupal.profile.fetch property not set, check the properties files for omissions or bad syntax</fail>
      </then>
      </if>
    
      <if>
       <not><isset property="drupal.settings.location"/></not>
      <then>
       <fail>drupal.settings.location property not set, check the properties files for omissions or bad syntax</fail>
      </then>
      </if>
      
      <if>
       <not><isset property="drupal.database.Url"/></not>
      <then>
       <fail>drupal.database.Url property not set, check the properties files for omissions or bad syntax</fail>
      </then>
      </if>
    
      <!--Get the root of the dir specified - this will be used as the working directory for the drupal module -->
      <php expression = "realpath('${drupal.dir}/../')" returnProperty="drupal.dir.root" />
      <!--local dir passed as -d option to CVS -->
      <php expression = "basename('${drupal.dir}')" returnProperty="drupal.dir.basename" />

      <!--     
      <if>
        <not><isset property="drupal.profile.replace_tokens"/></not>
        <then>
         <fail>drupal.profile.replace_tokens not set, check the properties files for omissions or bad syntax</fail>
         <if><not><isset property="drupal.profile.tokenizer_script"/></not><then><fail>drupal.profile.tokenizer_script not set, check the properties files for omissions or bad syntax</fail></then></if>
          <if><not><isset property="drupal.profile.token_properties_file"/></not><then><fail>drupal.profile.token_properties_file not set, check the properties files for omissions or bad syntax</fail></then></if> 
        </then>
        </if>  
    -->  
    </target>

  <!-- ================================= 
          target: dump-config              
         ================================= -->
  <target name="dump-config" depends="configure" description="--> Dumps all the current properties configuring the build to the screen.">
  <DumpProperties />      
  </target>

</project>