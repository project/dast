<?xml version="1.0" encoding="UTF-8"?>
<!-- <![CDATA[ 
]]> -->
<!--========================================================================-->
<!-- @file This is the build file for the default-site build project. It is
 configured with  the default-site-base.properties and                  
 default-site.properties files. This build file can be used directly as
 part of other build projects or by itself for basic site building.     
 This build project executes three main operations:
 1. Download Drupal core and specified modules from CVS for a given revision
    into drupal.dir.
 2. Create a MySQL database and login for use by the Drupal site using 
    drupal.database.* properties and points db_url in settings.php to the newly
    created database.
 3. Downloads and runs the installer with the default profile.

  @author Allister Beharry
  @package org.drupal.dast.base
-->
<!--========================================================================-->
<project name="default-site" default="main">  
  <taskdef name="CreoleSQLExec" classname="phing.tasks.ext.CreoleSQLExecTask"/>
  <taskdef name="CVS" classname="phing.tasks.system.CVSTask"/>
  <taskdef name="warn" classname="phing.tasks.system.WarnTask"/>
  <taskdef name="DumpProperties" classname="DumpPropertiesTask"/>

  <!-- ================================= 
       target: clean-files              
       ================================= -->
  <target name="clean-files" depends="configure" description="--> standalone task to remove the files directory under drupal.dir">
    <echo>-------------------------------------------------</echo>
    <echo> +++++ Running default-site.xml clean-files +++++</echo>
    <echo>-------------------------------------------------</echo>  
    <available file="${drupal.filesDirPath}" type="dir" property="drupal.filesDirPathExists"/>
    <if><isset property="drupal.filesDirPathExists"/><then>
        <delete dir="${drupal.filesDirPath}" includeemptydirs="true" verbose="false" failonerror="true"/>
        <echo>${drupal.filesDirPath} deleted </echo>
      </then>
    </if>        
    </target> 
    
  <!-- ================================= 
          target: main              
         ================================= -->
  <target name="main" depends="configure" description="--> Runs
the site-configure, site-build scripts in sequence">

    <!--Clean site directory if requested -->
    <if><equals arg1="${build.run.clean}" arg2="yes"/><then>
      <phing phingfile="${dast.home}${php.directory_separator}site-fetch.xml"
       inheritAll = "true" target="clean" haltonfailure="true" />
    </then>
    </if>

    <!--Fetch the Drupal core for the site -->
    <phing phingfile="${dast.home}${php.directory_separator}site-fetch.xml"
     inheritAll = "true" target="fetchCore" haltonfailure="true"/>

    <!--Fetch modules -->
    <phing phingfile="${dast.home}${php.directory_separator}site-fetch.xml"
     inheritAll = "true" target="fetchModules" haltonfailure="true"/>

    <!--Create the site database -->
    <phing phingfile="${dast.home}${php.directory_separator}site-configure.xml"
     inheritAll = "true" target="create-database" haltonfailure="true"/>

    <!--Set the db-url, fetch and start the installation profile -->
    <phing phingfile="${dast.home}${php.directory_separator}site-install.xml"
     inheritAll = "true" target="set-db-url" haltonfailure="true"/>
    <phing phingfile="${dast.home}${php.directory_separator}site-install.xml"
     inheritAll = "true" target="fetch-profile" haltonfailure="true"/>
    <phing phingfile="${dast.home}${php.directory_separator}site-install.xml"
     inheritAll = "true" target="run-installer" haltonfailure="true"/>

  </target>
  <!-- ================================= 
        target: configure              
       ================================= -->
<target name="configure" description="--> Loads properties for the build from properties files.">
  <if><isset property="basePropertiesFile"/><then>
    <available file="${project.basedir}${php.directory_separator}${basePropertiesFile}" property="basePropertiesFileExists"/>
      <if><not><isset property="basePropertiesFileExists"/></not><then>
         <fail>Could not find the specified base properties file ${project.basedir}${php.directory_separator}${basePropertiesFile}. Stopping.</fail>
      </then>
      <else>
        <property name="build.basePropertiesFile" value="${project.basedir}${php.directory_separator}${basePropertiesFile}" />
        <echo>Using ${build.basePropertiesFile} as base properties file.</echo>
      </else>  
      </if>
     </then>
    
    <else> <!--Use default base properties file -->
      <available file="${project.basedir}${php.directory_separator}default-site-base.properties" property="basePropertiesFileExists"/>
      <if><not><isset property="basePropertiesFileExists"/></not><then> 
        <warn>Could not find the default base properties file ${project.basedir}${php.directory_separator}default-site-base.properties. Only the user properties file can be used.</warn>
      </then>
      <else>
        <property name="build.basePropertiesFile" value="${project.basedir}${php.directory_separator}default-site-base.properties" />
        <echo>Using default base properties file ${build.basePropertiesFile}.</echo>
      </else>  
      </if>
    </else>  
    </if>
  
  <if><isset property="propertiesFile"/><then> 
    <!--Use user properties from specified file -->
    <available file="${project.basedir}${php.directory_separator}${propertiesFile}" property="propertiesFileExists"/>
    <if><not><isset property="propertiesFileExists"/></not><then>
      <fail>Could not find the specified user properties file ${project.basedir}${php.directory_separator}${propertiesFile}. Stopping.</fail>
    </then>
    <else>
      <property name="build.propertiesFile" value="${project.basedir}${php.directory_separator}${propertiesFile}" />
      <echo>Using ${build.propertiesFile} as user properties file.</echo>
    </else>
    </if>
  </then>
    
  <else> <!--Use default user properties file --> 
    <available file="${project.basedir}${php.directory_separator}default-site.properties" property="propertiesFileExists"/>
    <if><not><isset property="propertiesFileExists"/></not><then> 
      <warn>Could not find the default user properties file ${project.basedir}${php.directory_separator}default-site.properties. Only the base properties file will be used.</warn>
    </then>
    <else>
      <property name="build.propertiesFile" value="${project.basedir}${php.directory_separator}default-site.properties" />
      <echo>Using ${build.propertiesFile} as default user properties file.</echo>      
    </else>  
    </if>  
  </else>
  </if>
    
 
  <if><and><not><isset property="build.basePropertiesFile" /></not><not><isset property="build.propertiesFile" /></not></and><then>
    <fail>Cannot use either the user or base properties files. Stopping </fail>
  </then>
  </if>
  
  <if><isset property="build.basePropertiesFile" /><then>
    <property file="${build.basePropertiesFile}" override="true"/>
  </then>
   <else><echo>No base properties file used.</echo></else>
  </if>
  
  <if><isset property="build.propertiesFile" /><then>
    <if><equals arg1="${build.propertiesFile}" arg2="${dast.home}${php.directory_separator}default-site.properties" /><then>
      <if><and><isset property="build.basePropertiesFile" /><not><equals arg1="${build.basePropertiesFile}" arg2="${dast.home}${php.directory_separator}default-site-base.properties" /></not></and><then>
        <echo>The base properties file ${build.basePropertiesFile} is not the default so the default user properties file will not be loaded.</echo>
      </then>
      <else>   
        <property file="${build.propertiesFile}" override="true"/>
        <echo>Using default user properties file ${dast.home}${php.directory_separator}default-site.properties.</echo>
      </else>
      </if>  
    </then>
    <else><property file="${build.propertiesFile}" override="true"/></else>
    </if>
  </then>  
  <else><echo>No user properties file used.</echo></else>
  </if>
  
  <if><not><isset property="build.log.defaultdir"/></not><then>
    <fail>build.log.defaultdir property not set, check the ${propertiesFile} file for omissions or bad syntax</fail>
  </then>
  </if>
  <available file="${build.log.defaultdir}" property="build.log.defaultdirExists"/>
  <if><not><isset property="build.log.defaultdirExists"/></not><then>
   <echo>${build.log.defaultdir} doesn't exist, attempting to create...</echo>
   <mkdir dir="${build.log.defaultdir}"/>
  </then>
  </if>
  
  <if><not><isset property="build.run.clean"/></not><then>
    <fail>build.run.clean property not set, check the properties file for omissions or bad syntax</fail>
  </then>
  </if>

  <property name="drupal.database.Url" value="${drupal.database.driver}://${drupal.database.user}:${drupal.database.user.pass}@${drupal.database.host}/${drupal.database.name}" />
  <property name="drupal.database.CreateUrl" value="${drupal.database.driver}://${drupal.database.CreateScript.user}:${drupal.database.CreateScript.user.pass}@${drupal.database.host}/${drupal.database.CreateScript.name}" /> 
</target>
  
<!-- ================================= 
        target: dump-config              
       ================================= -->
<target name="dump-config" depends="configure" description="--> Dumps all the properties configuring the build to screen">
  <DumpProperties />      
</target>


</project>